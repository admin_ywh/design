package com.example.administrator.design.ui.main.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.administrator.design.R;
import com.example.administrator.design.bean.TripDetailsBean;

import java.util.List;

public class TripDetailsAdapter extends BaseQuickAdapter<TripDetailsBean.PlanScenicAreaListBean, BaseViewHolder> {
    public TripDetailsAdapter(@Nullable List<TripDetailsBean.PlanScenicAreaListBean> data) {
        super(R.layout.layout_trip_details_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TripDetailsBean.PlanScenicAreaListBean item) {
        helper.setText(R.id.tv_time, item.getSetOutTime());
        ImageView ivIcon = helper.getView(R.id.iv_pic);
        Glide.with(mContext).load(item.getImgUrl()).into(ivIcon);
        helper.setText(R.id.tv_title, item.getScenicAreaName());
        helper.addOnClickListener(R.id.iv_pic);
        helper.setText(R.id.tv_sign, "Sign" + item.getLanscapeMarks());
        helper.setText(R.id.tv_yule, "Enter" + item.getEntertainmentMarks());
        helper.setText(R.id.tv_personal, "Humanity" + item.getCulturalMarks());
        helper.setText(R.id.tv_day, "PlayTimes" + item.getPlayTimes());
    }
}
