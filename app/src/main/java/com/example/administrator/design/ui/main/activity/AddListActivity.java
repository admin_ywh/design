package com.example.administrator.design.ui.main.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.administrator.design.MainActivity;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.bean.ScenicSpotBean;
import com.example.administrator.design.ui.login.activity.LoginActivity;
import com.example.administrator.design.ui.main.adapter.AddAdapter;
import com.example.administrator.design.ui.main.contacts.AddContact;
import com.example.administrator.design.ui.main.presenter.AddPresenter;
import com.example.administrator.design.ui.mine.activity.MyTripActivity;
import com.example.administrator.design.utils.ActivityManager;
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;
import com.lcodecore.tkrefreshlayout.header.progresslayout.ProgressLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddListActivity extends BaseActivity<AddContact.presenter> implements
        RadioGroup.OnCheckedChangeListener, AddContact.view {


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_product)
    TextView tvProject;
    @BindView(R.id.rb_item2)
    RadioButton rbItem2;
    @BindView(R.id.rb_item3)
    RadioButton rbItem3;
    @BindView(R.id.rb_item4)
    RadioButton rbItem4;
    @BindView(R.id.rp_group)
    RadioGroup rpGroup;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.mRefresh)
    TwinklingRefreshLayout mRefresh;
    private String title, sum, time;
    private AddAdapter addAdapter;
    private List<String> list = new ArrayList<>();
    private String type = "1";
    private String cityID;
    private List<ScenicSpotBean.ScenicspotBean> data = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_list;
    }

    @Override
    public AddContact.presenter initPresenter() {
        return new AddPresenter(this);
    }

    @Override
    protected void initViews() {

        title = getIntent().getStringExtra("title");
        sum = getIntent().getStringExtra("sum");
        time = getIntent().getStringExtra("time");
        cityID = getIntent().getStringExtra("cityId");
        if (cityID == null || cityID.equals("")) {
            tvProject.setVisibility(View.VISIBLE);
        }else {
            tvProject.setVisibility(View.GONE);
        }
        ProgressLayout headView = new ProgressLayout(this);
        headView.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent), ContextCompat.getColor(this, R.color.yellow),
                ContextCompat.getColor(this, R.color.Green));
        mRefresh.setHeaderView(headView);
        mRefresh.setOnRefreshListener(new RefreshListenerAdapter() {
            @Override
            public void onRefresh(TwinklingRefreshLayout refreshLayout) {
                super.onRefresh(refreshLayout);
                presenter.onRefresh();
            }

            @Override
            public void onLoadMore(TwinklingRefreshLayout refreshLayout) {
                super.onLoadMore(refreshLayout);
                new Handler().postDelayed(() -> refreshLayout.finishLoadmore(), 1500);
            }
        });


        rpGroup.setOnCheckedChangeListener(this);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        addAdapter = new AddAdapter(data);
        rvList.setAdapter(addAdapter);
        addAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                Button button;
                button = view.findViewById(R.id.btn_want);
                if (view.getId() == R.id.btn_want) {
                    if (button.getText().toString().equals("want")) {
                        list.add(addAdapter.getItem(position).getId() + "");
                        button.setText("no want");
                    } else {
                        list.remove(addAdapter.getItem(position).getId() + "");
                        button.setText("want");
                    }
                } else if (view.getId() == R.id.iv_pic) {
                    startActivity(new Intent(AddListActivity.this, SceneryActivity.class)
                            .putExtra("imageUrl", addAdapter.getItem(position).getImgUrls())
                            .putExtra("content", addAdapter.getItem(position).getDescription()));
                }

            }
        });
    }

    @OnClick({R.id.iv_back, R.id.tv_product})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_product:
                presenter.addTrop();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checked) {
        switch (checked) {
            case R.id.rb_item2:
                type = "2";
                presenter.getTripList();
                break;
            case R.id.rb_item3:
                type = "3";
                presenter.getTripList();
                break;
            case R.id.rb_item4:
                type = "4";
                presenter.getTripList();
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getTripList();
    }

    @Override
    public void setDataSpot(List<ScenicSpotBean.ScenicspotBean> scenicspot) {
        addAdapter.setNewData(scenicspot);
    }

    @Override
    public String getPlanName() {
        return title;
    }

    @Override
    public String getScenicSpotIds() {
        return ArrayList2String(list);
    }

    @Override
    public String getOutTime() {
        return time;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void againLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        ActivityManager.getAppInstance().finishAllActivity();
    }

    @Override
    public String getCityId() {
        return cityID == null ? "" : cityID;
    }

    @Override
    public void OnRefreshSuccess() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mRefresh.finishRefreshing();
            }
        }, 1500);

    }

    /**
     *  提交成功跳转页面
     */
    @Override
    public void addTripSuccess() {
        startActivity(new Intent(this, MyTripActivity.class));

    }

    // ArrayList类型转成String类型
    public String ArrayList2String(List<String> arrayList) {
        String result = "";
        if (arrayList != null && arrayList.size() > 0) {
            for (String item : arrayList) {
                // 把列表中的每条数据用逗号分割开来，然后拼接成字符串
                result += item + ",";
            }
            // 去掉最后一个逗号
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

}
