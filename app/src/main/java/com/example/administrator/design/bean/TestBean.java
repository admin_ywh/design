package com.example.administrator.design.bean;

import java.io.Serializable;

/*
 * 项目名:    BaseFrame
 * 包名       com.zhon.frame.mvp.login.bean
 * 文件名:    TestBean
 * 创建者:    ZJB
 * 创建时间:  2017/9/7 on 11:11
 * 描述:     TODO
 */
public class TestBean implements Serializable{


    /**
     * headImg :
     * nickName : 测试用户
     * cellphone : 13812345678
     * userId : 2
     * token : 5C810EE7FC15E4B460F596F5578812A8
     */

    private String headImg;
    private String nickName;
    private String cellphone;
    private int userId;
    private String sex;
    private String token;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
