package com.example.administrator.design.ui.main.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.base.mvpbase.baseImpl.BasePresenterImpl;
import com.example.administrator.design.retroft.Api;
import com.example.administrator.design.retroft.ExceptionHelper;
import com.example.administrator.design.ui.main.contacts.CreateTripContact;
import com.orhanobut.hawk.Hawk;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CreateTripPresenter extends BasePresenterImpl<CreateTripContact.view> implements CreateTripContact.presenter {

    public CreateTripPresenter(CreateTripContact.view view) {
        super(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void crateTrip() {
        Api.getInstance().addTrip1(Hawk.get("token"),view.getPlanName(),view.getOutTime(),
                view.getDays(),view.getplayTimes(),view.getCityId(),view.getfirstImportant(),
                view.getsecondImportant(),view.getthirdImportant())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoadingDialog();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createBeanBaseBean -> {
                    view.dismissLoadingDialog();
                    //处理服务器返回的数据
                    if (createBeanBaseBean.getCode().equals("0"))
                    {
                        //处理服务器返回的错误信息
                        view.success(createBeanBaseBean.getData().getPlanId());
                        ToastUtils.showShort(createBeanBaseBean.getMsg());
                    } else if(createBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }{
                        Log.e("Throwable", createBeanBaseBean.getMsg());
                        ToastUtils.showShort(createBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    view.dismissLoadingDialog();
                    ExceptionHelper.handleException(throwable);
                });
    }
}
