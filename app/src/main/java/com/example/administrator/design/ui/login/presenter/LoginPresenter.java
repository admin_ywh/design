package com.example.administrator.design.ui.login.presenter;


import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BasePresenterImpl;
import com.example.administrator.design.retroft.Api;
import com.example.administrator.design.retroft.ExceptionHelper;
import com.example.administrator.design.ui.login.contact.LoginContact;
import com.example.administrator.design.utils.SharePreUtils;
import com.orhanobut.hawk.Hawk;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/*
 * 项目名:    BaseFrame
 * 包名       com.zhon.frame.mvp.login.presenter
 * 文件名:    LoginPresenter
 * 创建者:    ZJB
 * 创建时间:  2017/9/7 on 11:17
 * 描述:     TODO
 */
public class LoginPresenter extends BasePresenterImpl<LoginContact.view>
        implements LoginContact.presenter {
    public LoginPresenter(LoginContact.view view) {
        super(view);
    }
    @SuppressLint("CheckResult")
    @Override
    public void login() {
        String useName=view.getUserName();
        String password=view.getPsd();
        if(TextUtils.isEmpty(useName)||TextUtils.isEmpty(password)){
            ToastUtils.showShort(R.string.string_usename_null);
        }else {
            Api.getInstance().login(view.getUserName(), view.getPsd())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe(disposable -> {
                        addDisposable(disposable);
                        view.showLoadingDialog();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(storiesBeen -> {
                        view.dismissLoadingDialog();
                        //处理服务器返回的数据
                        if (storiesBeen.getCode().equals("0"))
                        {
                            Hawk.put("token",storiesBeen.getData().getToken());
                            view.getLoadData(storiesBeen.getData());
                            //处理服务器返回的错误信息
                        } else {
                            Log.e("Throwable", storiesBeen.getMsg());
                            ToastUtils.showShort(storiesBeen.getMsg());
                        }
                        //处理网络异常
                    }, throwable -> {
                        view.dismissLoadingDialog();
                        ExceptionHelper.handleException(throwable);
                    });
        }
    }

}
