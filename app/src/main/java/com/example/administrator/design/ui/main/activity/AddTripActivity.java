package com.example.administrator.design.ui.main.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.ui.main.TripDetailsActivity;
import com.example.administrator.design.ui.main.contacts.CreateTripContact;
import com.example.administrator.design.ui.main.presenter.CreateTripPresenter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class AddTripActivity extends BaseActivity<CreateTripContact.presenter>implements CreateTripContact.view {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.rl_top)
    RelativeLayout rlTop;
    @BindView(R.id.btn_add)
    Button btnAdd;
    @BindView(R.id.title)
    EditText tvTitle;
    @BindView(R.id.days)
    EditText tvDays;
    @BindView(R.id.time)
    TextView tvTime;
    @BindView(R.id.et_everyDay)
    EditText etEveryDay;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.spinner1)
    Spinner spinner1;
    @BindView(R.id.spinner2)
    Spinner spinner2;
    @BindView(R.id.spinner3)
    Spinner spinner3;
    private String cityId;
    boolean isSpinnerFirst = true ;
    private int firstImportant,secondImportant,thirdImportant;
    //定义一个String类型的List数组作为数据源
    private List<String> dataList1 = new ArrayList<>();

    //定义一个ArrayAdapter适配器作为spinner的数据适配器
    private ArrayAdapter<String> adapter, adapter1, adapter2;

    @Override
    public int getLayoutId() {
        return R.layout.activity_add_trip;
    }

    @Override
    public CreateTripContact.presenter initPresenter() {
        return new CreateTripPresenter(this);
    }

    @Override
    protected void initViews() {
        dataList1.add("Scenery");
        dataList1.add("humanity");
        dataList1.add("entertainment");
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dataList1);
        adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dataList1);
        adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dataList1);
        //为适配器设置下拉列表下拉时的菜单样式。
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //为spinner绑定我们定义好的数据适配器
        spinner1.setAdapter(adapter);
        spinner2.setAdapter(adapter);
        spinner3.setAdapter(adapter);
        //为spinner绑定监听器，这里我们使用匿名内部类的方式实现监听器
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if(isSpinnerFirst){
//                    //第一次初始化spinner时，不显示默认被选择的第一项即可
//                    view.setVisibility(View.INVISIBLE) ;
//                }
//                isSpinnerFirst=false;
                firstImportant= position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //为spinner绑定监听器，这里我们使用匿名内部类的方式实现监听器
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                secondImportant= position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //为spinner绑定监听器，这里我们使用匿名内部类的方式实现监听器
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                thirdImportant=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @OnClick({R.id.iv_back, R.id.btn_add, R.id.time, R.id.tv_city})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_city:
                startActivityForResult(new Intent(AddTripActivity.this, AddCityActivity.class), 0);
                break;
            case R.id.btn_add:
                if (!TextUtils.isEmpty(tvTitle.getText().toString().trim()) &&
                        !TextUtils.isEmpty(tvDays.getText().toString().trim()) &&
                        !TextUtils.isEmpty(tvTime.getText().toString().trim())) {
                    presenter.crateTrip();
                } else {
                    ToastUtils.showShort("Input can not be empty");
                }
                break;
            case R.id.time:
                TimePickerView pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
                        tvTime.setText(getTime(date));
                    }
                })
                        .setType(new boolean[]{true, true, true, false, false, false})// 默认全部显示
                        .setCancelText("Cancel")//取消按钮文字
                        .setSubmitText("Confirm")//确认按钮文字
                        .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .build();

                pvTime.show();
                break;
        }
    }

    private String getTime(Date date) {//可根据需要自行截取数据显示
        Log.d("getTime()", "choice date millis: " + date.getTime());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && data != null) {
            tvCity.setText("City："+data.getExtras().getString("cityName"));
            cityId=data.getExtras().get("cityId").toString();
        }
    }

    @Override
    public String getPlanName() {
        return tvTitle.getText().toString();
    }

    @Override
    public String getOutTime() {
        return tvTime.getText().toString();
    }

    @Override
    public String getDays() {
        return tvDays.getText().toString();
    }

    @Override
    public String getplayTimes() {
        return etEveryDay.getText().toString().trim();
    }

    @Override
    public String getCityId() {
        return cityId;
    }



    @Override
    public void againLogin() {

    }

    @Override
    public int getfirstImportant() {
        return firstImportant;
    }

    @Override
    public int getsecondImportant() {
        return secondImportant;
    }

    @Override
    public int getthirdImportant() {
        return thirdImportant;
    }

    /**
     *  添加行程成功回调
     * @param planId
     */
    @Override
    public void success(int planId) {
         startActivity(new Intent(this,TripDetailsActivity.class)
         .putExtra("planId",planId));
    }
}
