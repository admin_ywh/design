package com.example.administrator.design.ui.main.adapter;

import android.content.Context;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.administrator.design.R;
import com.example.administrator.design.bean.CityBean;
import com.example.administrator.design.ui.main.listener.OnItemClickLitener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class RadioCityAdapter extends RecyclerView.Adapter {

    private List<CityBean.CitysBean> datas;

    private int selected = -1;

    public RadioCityAdapter(List<CityBean.CitysBean> datas) {
        this.datas = datas;
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    public void setSelection(int position) {
        this.selected = position;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_radio_itey_item, parent, false);

        return new SingleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof SingleViewHolder) {
            final SingleViewHolder viewHolder = (SingleViewHolder) holder;
            String name = datas.get(position).getCityName();
            viewHolder.mTvName.setText(name);

            if (selected == position) {
                viewHolder.mCheckBox.setChecked(true);
                viewHolder.itemView.setSelected(true);
            } else {
                viewHolder.mCheckBox.setChecked(false);
                viewHolder.itemView.setSelected(false);
            }

            if (mOnItemClickLitener != null) {
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnItemClickLitener.onItemClick(viewHolder.itemView, viewHolder.getAdapterPosition(), datas.get(viewHolder.getAdapterPosition()).getCityName(),datas.get(position).getCityId());
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class SingleViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_select;
        TextView mTvName;
        CheckBox mCheckBox;

        public SingleViewHolder(View itemView) {
            super(itemView);
            rl_select=itemView.findViewById(R.id.rl_select);
            mTvName = (TextView) itemView.findViewById(R.id.tv_name);
            mCheckBox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }
    }

}