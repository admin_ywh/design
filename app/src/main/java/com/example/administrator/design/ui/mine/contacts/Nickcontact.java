package com.example.administrator.design.ui.mine.contacts;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;
import com.example.administrator.design.bean.UpdateUserBean;

public interface Nickcontact {

    interface view extends BaseView{

        String getNickName();

        void Successs(UpdateUserBean data);
    }
    interface presenter extends BasePresenter{

        void setUserName();
    }
}
