package com.example.administrator.design.ui.main.activity;


import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.administrator.design.MainActivity;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.bean.CityBean;
import com.example.administrator.design.ui.main.adapter.SelectCityAdapter;
import com.example.administrator.design.ui.main.contacts.SelectCityContact;
import com.example.administrator.design.ui.main.presenter.SelectCityPresenter;
import com.orhanobut.hawk.Hawk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class SelectCityActivity extends BaseActivity<SelectCityContact.presenter>
        implements SelectCityContact.homeView,SelectCityAdapter.OnClickListener{


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.rl_top)
    RelativeLayout rlTop;
    @BindView(R.id.city_list)
    RecyclerView cityList;
    @BindView(R.id.tv_save)
    TextView tvSave;

    private SelectCityAdapter cityAdapter;
    private List<CityBean.CitysBean> data = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_select_city;
    }

    @Override
    public SelectCityContact.presenter initPresenter() {
        return new SelectCityPresenter(this);
    }


    @Override
    protected void initViews() {
        cityList.setLayoutManager(new LinearLayoutManager(this));
        cityAdapter = new SelectCityAdapter(this,data);
        cityList.setAdapter(cityAdapter);

    }


    @Override
    public String getToken() {
        return Hawk.get("token");
    }

    @Override
    public void LoadData(CityBean bean) {
        data.clear();
        for (int i = 0; i < bean.getCitys().size(); i++) {
            data.add(bean.getCitys().get(i));
        }
        cityAdapter.notifyDataSetChanged();
    }

    @Override
    public void success() {
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.city();
    }



    @OnClick({R.id.iv_back, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_save:
                cityAdapter.getSelectedItem();
                cityAdapter.notifyDataSetChanged();
                presenter.city();
                break;
        }
    }

    @Override
    public void setData(List<CityBean.CitysBean> list) {
       presenter.submitCityID(list);
    }
}
