package com.example.administrator.design.ui.main.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseFragment;
import com.example.administrator.design.bean.TestBean;
import com.example.administrator.design.ui.login.activity.LoginActivity;
import com.example.administrator.design.ui.mine.activity.MyTripActivity;
import com.example.administrator.design.ui.mine.activity.NickNameActivity;
import com.example.administrator.design.ui.mine.activity.OpinionActivity;
import com.example.administrator.design.ui.mine.contacts.MineContact;
import com.example.administrator.design.ui.mine.presenter.MinePresenter;
import com.example.administrator.design.utils.ActivityManager;
import com.example.administrator.design.view.RoundImageView;
import com.example.administrator.design.view.SexDialog;
import com.example.administrator.design.view.ZDialog;
import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.OnClick;


public class MineFragment extends BaseFragment<MineContact.presenter>
        implements MineContact.view{


    @BindView(R.id.iv_round)
    RoundImageView ivRound;
    @BindView(R.id.rl_personal)
    RelativeLayout rlPersonal;
    @BindView(R.id.rl_city)
    RelativeLayout rlCity;
    @BindView(R.id.rl_question)
    RelativeLayout rlQuestion;
    @BindView(R.id.rl_version)
    RelativeLayout rlVersion;
    @BindView(R.id.btn_outlogin)
    Button btnOutlogin;
    @BindView(R.id.rl_top)
    RelativeLayout rlTop;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.sex)
    TextView sex;
    @BindView(R.id.iv_mypwd)
    ImageView ivMypwd;
    @BindView(R.id.iv_city)
    ImageView ivCity;
    @BindView(R.id.iv_question)
    ImageView ivQuestion;
    @BindView(R.id.iv_version)
    ImageView ivVersion;

    private TestBean bean;
    private SexDialog dialog;
    private ZDialog zDialog;
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initView() {
        dialog=new SexDialog(getContext(),R.style.MyDialog);
        initData();
    }

    private void initData() {
        bean = Hawk.get("testBean");
        username.setText(bean.getNickName());
        sex.setText("Sex:" + bean.getSex());
        dialog.setOnCheckedChangeListener(new SexDialog.OnCheckedChangeListener() {
            @Override
            public void OnCheckedChangeListener(int position) {
               if(position==0){
                   presenter.updateUser(position);
                   dialog.dismiss();
               } else {
                   presenter.updateUser(position);
                   dialog.dismiss();
               }
            }
        });
    }

    @Override
    public MineContact.presenter initPresenter() {
        return new MinePresenter(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @OnClick({R.id.rl_personal, R.id.rl_city, R.id.rl_question, R.id.rl_version,
            R.id.btn_outlogin,R.id.iv_round, R.id.username, R.id.sex})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_personal:
                break;
                //我的行程
            case R.id.rl_city:
                startActivity(new Intent(getContext(), MyTripActivity.class));
                break;
            case R.id.rl_question:
                startActivity(new Intent(getContext(), OpinionActivity.class));
                break;
            case R.id.rl_version:
                ToastUtils.showShort("It's the latest version");
                break;
            case R.id.btn_outlogin:
                Hawk.delete("token");
                zDialog=new ZDialog(getContext(),"cancel","Are you sure you want to quit logon","confirm",R.style.MyDialog);
                zDialog.show();
                zDialog.setOnclickListener(new ZDialog.OnclickListener() {
                    @Override
                    public void OnClickListener() {
                        ActivityManager.getAppInstance().finishAllActivity();
                        Hawk.delete("token");
                        startActivity(new Intent(getContext(), LoginActivity.class));
                    }
                });

                break;
            case R.id.iv_round:
                break;
            case R.id.username:
               startActivityForResult(new Intent(getContext(), NickNameActivity.class),1);
                break;
            case R.id.sex:
                dialog.show();
                break;
        }
    }

    @Override
    public void Successs(int data) {
         if(data==0){
             sex.setText("sex: male");
         }else {
             sex.setText("sex: female");
         }
    }

    @Override
    public void againLogin() {
        startActivity(new Intent(getContext(),LoginActivity.class));
        ActivityManager.getAppInstance().finishAllActivity();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null){
            username.setText(data.getExtras().getString("NickName"));
        }
    }
}
