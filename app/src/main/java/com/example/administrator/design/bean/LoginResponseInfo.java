package com.example.administrator.design.bean;

import java.io.Serializable;

/**
 * Created by xiaochen on 2017/2/22.
 * 返回的用户信息，不是客户
 *
 * @email xiaochenu@gmail.com
 */

public class LoginResponseInfo extends BaseInfo {
    public User user_info;

    public User getUser_info() {
        return user_info;
    }

    public void setUser_info(User user_info) {
        this.user_info = user_info;
    }

    public class User implements Serializable {
        private String id;
        private String phone;
        private String name;
        private String passwd;
        private String gesture_pwd;
        private String gender;
        private String job_number;
        private String email;
        private String qq;
        private String portrait;
        private String address;
        private String team;
        private String ip;
        private String add_time;
        private String status;
        private String last_time;
        private String last_ip;
        private String role_id;
        private String platform_type;
        private String device_token;
        private String login_count;

        public User(String id, String phone, String name, String passwd, String gesture_pwd, String gender, String job_number, String email, String qq, String portrait, String address, String team, String ip, String add_time, String status, String last_time, String last_ip, String role_id, String platform_type, String device_token, String login_count) {
            this.id = id;
            this.phone = phone;
            this.name = name;
            this.passwd = passwd;
            this.gesture_pwd = gesture_pwd;
            this.gender = gender;
            this.job_number = job_number;
            this.email = email;
            this.qq = qq;
            this.portrait = portrait;
            this.address = address;
            this.team = team;
            this.ip = ip;
            this.add_time = add_time;
            this.status = status;
            this.last_time = last_time;
            this.last_ip = last_ip;
            this.role_id = role_id;
            this.platform_type = platform_type;
            this.device_token = device_token;
            this.login_count = login_count;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id='" + id + '\'' +
                    ", phone='" + phone + '\'' +
                    ", name='" + name + '\'' +
                    ", passwd='" + passwd + '\'' +
                    ", gesture_pwd='" + gesture_pwd + '\'' +
                    ", gender='" + gender + '\'' +
                    ", job_number='" + job_number + '\'' +
                    ", email='" + email + '\'' +
                    ", qq='" + qq + '\'' +
                    ", portrait='" + portrait + '\'' +
                    ", address='" + address + '\'' +
                    ", team='" + team + '\'' +
                    ", ip='" + ip + '\'' +
                    ", add_time='" + add_time + '\'' +
                    ", status='" + status + '\'' +
                    ", last_time='" + last_time + '\'' +
                    ", last_ip='" + last_ip + '\'' +
                    ", role_id='" + role_id + '\'' +
                    ", platform_type='" + platform_type + '\'' +
                    ", device_token='" + device_token + '\'' +
                    ", login_count='" + login_count + '\'' +
                    '}';
        }

        public User() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPasswd() {
            return passwd;
        }

        public void setPasswd(String passwd) {
            this.passwd = passwd;
        }

        public String getGesture_pwd() {
            return gesture_pwd;
        }

        public void setGesture_pwd(String gesture_pwd) {
            this.gesture_pwd = gesture_pwd;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getJob_number() {
            return job_number;
        }

        public void setJob_number(String job_number) {
            this.job_number = job_number;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getQq() {
            return qq;
        }

        public void setQq(String qq) {
            this.qq = qq;
        }

        public String getPortrait() {
            return portrait;
        }

        public void setPortrait(String portrait) {
            this.portrait = portrait;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getTeam() {
            return team;
        }

        public void setTeam(String team) {
            this.team = team;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getLast_time() {
            return last_time;
        }

        public void setLast_time(String last_time) {
            this.last_time = last_time;
        }

        public String getLast_ip() {
            return last_ip;
        }

        public void setLast_ip(String last_ip) {
            this.last_ip = last_ip;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getPlatform_type() {
            return platform_type;
        }

        public void setPlatform_type(String platform_type) {
            this.platform_type = platform_type;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getLogin_count() {
            return login_count;
        }

        public void setLogin_count(String login_count) {
            this.login_count = login_count;
        }
    }
}
