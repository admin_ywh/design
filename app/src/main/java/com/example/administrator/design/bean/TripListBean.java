package com.example.administrator.design.bean;

import java.util.List;

public class TripListBean {

    private List<TravelPlanListBean> travelPlanList;

    public List<TravelPlanListBean> getTravelPlanList() {
        return travelPlanList;
    }

    public void setTravelPlanList(List<TravelPlanListBean> travelPlanList) {
        this.travelPlanList = travelPlanList;
    }

    public static class TravelPlanListBean {
        /**
         * imgUrl : http://www.gxdms.com/ueditor/php/upload/image/20170430/1493520382393310.jpeg
         * endPoint : 广州市
         * startPoint : 南宁市
         * planName : 国庆出游计划
         * planId : 1
         * time : 2018年10月1日-2018年10月7日
         */

        private String imgUrl;
        private String endPoint;
        private String startPoint;
        private String planName;
        private int planId;
        private String time;
        private String description;

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getEndPoint() {
            return endPoint;
        }

        public void setEndPoint(String endPoint) {
            this.endPoint = endPoint;
        }

        public String getStartPoint() {
            return startPoint;
        }

        public void setStartPoint(String startPoint) {
            this.startPoint = startPoint;
        }

        public String getPlanName() {
            return planName;
        }

        public void setPlanName(String planName) {
            this.planName = planName;
        }

        public int getPlanId() {
            return planId;
        }

        public void setPlanId(int planId) {
            this.planId = planId;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }
}
