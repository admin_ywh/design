/**
 * Copyright 2017 jb51.net
 */
package com.example.administrator.design.bean;

import java.io.Serializable;

/**
 * Auto-generated: 2018-07-10 16:20:50
 *
 * @author jb51.net (i@jb51.net)
 * @website http://tools.jb51.net/code/json2javabean
 */
public class GuestInfo implements Serializable {

    /**
     * id : 4643
     * phone : 18700000000
     * name : gg
     * tag : 0
     * team : 1
     * salesman : 221
     * controller : 0
     * group_pic : http://img.zwgt.com/customerimg/4643/1488278436group_pic.jpg
     * id_number : null
     * id_img_1 :
     * id_img_2 :
     * id_print_img : null
     * apply_pic :
     * apply_pic_back :
     * accumulation : null
     * salary : 0
     * apply_credit : 0
     * credit : 0
     * credit_log : 0
     * start_time : null
     * end_time : null
     * salary_pic :
     * married : 0
     * hometown : null
     * children : 0
     * address : null
     * education : 0
     * house : 0
     * company : null
     * company_type : 0
     * job_type : 0
     * professional_ranks : 0
     * title : null
     * level : 0
     * debt : 0
     * debt_amount : 0
     * work_tel : null
     * province : 1
     * city : 4
     * district : 8
     * work_address : null
     * work_period : 0
     * university : null
     * mate_name : null
     * mate_id_number : null
     * mate_phone : null
     * mate_company : null
     * mate_title : null
     * mate_salary : 0
     * estate_num : 0
     * estate_value : 0
     * estate_remain : 0
     * estate1_pic1 :
     * estate1_pic2 :
     * estate1_owner : null
     * estate1_address : null
     * estate1_area : 0
     * estate2_pic1 :
     * estate2_pic2 :
     * estate2_owner : null
     * estate2_address : null
     * estate2_area : 0
     * other_imgs : http://img.zwgt.com/customerimg/4643/1488272421more_pic0.jpg,,
     * car_number : null
     * car_type : null
     * car_pic :
     * car_value : 0
     * marriage_pic :
     * apply_cordinate :
     * group_cordinate : 120.1745083333333,30.21711333333333
     * signature_pic :
     * signature_coor :
     * add_score : 0
     * other : 0
     * progress : 1
     * status : 1
     * ctime : 1487919965
     * utime : 1490087323
     * sequence : null
     * commit_time : null
     * pass_time : null
     * uuid : 9dd008564599c7ca9b4c4d13c64173d8
     * sync_time : null
     * salesman_name : 安卓测试
     */

    public String id;
    public String phone;
    public String name;
    public String tag;
    public String team;
    public String salesman;
    public String controller;
    public String group_pic;
    public Object id_number;
    public String id_img_1;
    public String id_img_2;
    public Object id_print_img;
    public String apply_pic;
    public String apply_pic_back;
    public Object accumulation;
    public String salary;
    public String apply_credit;
    public String credit;
    public String credit_log;
    public Object start_time;
    public Object end_time;
    public String salary_pic;
    public String married;
    public Object hometown;
    public String children;
    public Object address;
    public String education;
    public String house;
    public Object company;
    public String company_type;
    public String job_type;
    public String professional_ranks;
    public Object title;
    public String level;
    public String debt;
    public String debt_amount;
    public Object work_tel;
    public String province;
    public String city;
    public String district;
    public Object work_address;
    public String work_period;
    public Object university;
    public Object mate_name;
    public Object mate_id_number;
    public Object mate_phone;
    public Object mate_company;
    public Object mate_title;
    public String mate_salary;
    public String estate_num;
    public String estate_value;
    public String estate_remain;
    public String estate1_pic1;
    public String estate1_pic2;
    public Object estate1_owner;
    public Object estate1_address;
    public String estate1_area;
    public String estate2_pic1;
    public String estate2_pic2;
    public Object estate2_owner;
    public Object estate2_address;
    public String estate2_area;
    public String other_imgs;
    public Object car_number;
    public Object car_type;
    public String car_pic;
    public String car_value;
    public String marriage_pic;
    public String apply_cordinate;
    public String group_cordinate;
    public String signature_pic;
    public String signature_coor;
    public String add_score;
    public String other;
    public String progress;
    public String status;
    public String ctime;
    public String utime;
    public Object sequence;
    public Object commit_time;
    public Object pass_time;
    public String uuid;
    public Object sync_time;
    public String salesman_name;

    @Override
    public String toString() {
        return "GuestInfo{" +
                "id='" + id + '\'' +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                ", tag='" + tag + '\'' +
                ", team='" + team + '\'' +
                ", salesman='" + salesman + '\'' +
                ", controller='" + controller + '\'' +
                ", group_pic='" + group_pic + '\'' +
                ", id_number=" + id_number +
                ", id_img_1='" + id_img_1 + '\'' +
                ", id_img_2='" + id_img_2 + '\'' +
                ", id_print_img=" + id_print_img +
                ", apply_pic='" + apply_pic + '\'' +
                ", apply_pic_back='" + apply_pic_back + '\'' +
                ", accumulation=" + accumulation +
                ", salary='" + salary + '\'' +
                ", apply_credit='" + apply_credit + '\'' +
                ", credit='" + credit + '\'' +
                ", credit_log='" + credit_log + '\'' +
                ", start_time=" + start_time +
                ", end_time=" + end_time +
                ", salary_pic='" + salary_pic + '\'' +
                ", married='" + married + '\'' +
                ", hometown=" + hometown +
                ", children='" + children + '\'' +
                ", address=" + address +
                ", education='" + education + '\'' +
                ", house='" + house + '\'' +
                ", company=" + company +
                ", company_type='" + company_type + '\'' +
                ", job_type='" + job_type + '\'' +
                ", professional_ranks='" + professional_ranks + '\'' +
                ", title=" + title +
                ", level='" + level + '\'' +
                ", debt='" + debt + '\'' +
                ", debt_amount='" + debt_amount + '\'' +
                ", work_tel=" + work_tel +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", work_address=" + work_address +
                ", work_period='" + work_period + '\'' +
                ", university=" + university +
                ", mate_name=" + mate_name +
                ", mate_id_number=" + mate_id_number +
                ", mate_phone=" + mate_phone +
                ", mate_company=" + mate_company +
                ", mate_title=" + mate_title +
                ", mate_salary='" + mate_salary + '\'' +
                ", estate_num='" + estate_num + '\'' +
                ", estate_value='" + estate_value + '\'' +
                ", estate_remain='" + estate_remain + '\'' +
                ", estate1_pic1='" + estate1_pic1 + '\'' +
                ", estate1_pic2='" + estate1_pic2 + '\'' +
                ", estate1_owner=" + estate1_owner +
                ", estate1_address=" + estate1_address +
                ", estate1_area='" + estate1_area + '\'' +
                ", estate2_pic1='" + estate2_pic1 + '\'' +
                ", estate2_pic2='" + estate2_pic2 + '\'' +
                ", estate2_owner=" + estate2_owner +
                ", estate2_address=" + estate2_address +
                ", estate2_area='" + estate2_area + '\'' +
                ", other_imgs='" + other_imgs + '\'' +
                ", car_number=" + car_number +
                ", car_type=" + car_type +
                ", car_pic='" + car_pic + '\'' +
                ", car_value='" + car_value + '\'' +
                ", marriage_pic='" + marriage_pic + '\'' +
                ", apply_cordinate='" + apply_cordinate + '\'' +
                ", group_cordinate='" + group_cordinate + '\'' +
                ", signature_pic='" + signature_pic + '\'' +
                ", signature_coor='" + signature_coor + '\'' +
                ", add_score='" + add_score + '\'' +
                ", other='" + other + '\'' +
                ", progress='" + progress + '\'' +
                ", status='" + status + '\'' +
                ", ctime='" + ctime + '\'' +
                ", utime='" + utime + '\'' +
                ", sequence=" + sequence +
                ", commit_time=" + commit_time +
                ", pass_time=" + pass_time +
                ", uuid='" + uuid + '\'' +
                ", sync_time=" + sync_time +
                ", salesman_name='" + salesman_name + '\'' +
                '}';
    }
}