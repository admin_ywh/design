package com.example.administrator.design.ui.main.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.base.mvpbase.baseImpl.BasePresenterImpl;
import com.example.administrator.design.retroft.Api;
import com.example.administrator.design.retroft.ExceptionHelper;
import com.example.administrator.design.ui.main.contacts.AddContact;
import com.orhanobut.hawk.Hawk;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AddPresenter extends BasePresenterImpl<AddContact.view> implements AddContact.presenter {
    String token= Hawk.get("token");
    public AddPresenter(AddContact.view view) {
        super(view);

    }

    @SuppressLint("CheckResult")
    @Override
    public void getTripList() {

        Api.getInstance().ScenicSpot(token,view.getType(),view.getCityId())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoadingDialog();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(secniBean -> {
                    view.dismissLoadingDialog();
                    //处理服务器返回的数据
                    if (secniBean.getCode().equals("0"))
                    {
                        //处理服务器返回的错误信息
                        view.setDataSpot(secniBean.getData().getScenicspot());
                    } else  if(secniBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }else {
                        Log.e("Throwable", secniBean.getMsg());
                        ToastUtils.showShort(secniBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    view.dismissLoadingDialog();
                    ExceptionHelper.handleException(throwable);
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void addTrop() {
        Api.getInstance().addTrip(token,view.getPlanName(),view.getScenicSpotIds(),view.getOutTime())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoadingDialog();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(addTripBeanBaseBean -> {
                    view.dismissLoadingDialog();
                    //处理服务器返回的数据
                    if (addTripBeanBaseBean.getCode().equals("0"))
                    {
                        //处理服务器返回的错误信息
                        view.addTripSuccess();
                        ToastUtils.showShort(addTripBeanBaseBean.getMsg());
                    } else if(addTripBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }{
                        Log.e("Throwable", addTripBeanBaseBean.getMsg());
                        ToastUtils.showShort(addTripBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    view.dismissLoadingDialog();
                    ExceptionHelper.handleException(throwable);
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void onRefresh() {
        Api.getInstance().ScenicSpot(token,view.getType(),view.getCityId())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoadingDialog();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(secniBean -> {
                    view.dismissLoadingDialog();
                    //处理服务器返回的数据
                    if (secniBean.getCode().equals("0"))
                    {
                        //处理服务器返回的错误信息
                        view.setDataSpot(secniBean.getData().getScenicspot());
                        view.OnRefreshSuccess();
                    } else  if(secniBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }else {
                        Log.e("Throwable", secniBean.getMsg());
                        ToastUtils.showShort(secniBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    view.dismissLoadingDialog();
                    ExceptionHelper.handleException(throwable);
                });
    }
}
