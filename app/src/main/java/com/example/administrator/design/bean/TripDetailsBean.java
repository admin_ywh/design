package com.example.administrator.design.bean;

import java.util.List;

public class TripDetailsBean {

    /**
     * joinUserList : []
     * secondImportant : cultural
     * planScenicAreaList : [{"imgUrl":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1534211961060&di=ebfef2597b05288b9959668ffed672f6&imgtype=0&src=http://img.youtx.com/guide/201306/17/596/5fdff50ac112a218f50d7d7cdf061945.jpeg","lanscapeMarks":10,"entertainmentMarks":9,"description":"The Belvedere palaces were the summer residence of Prince Eugene of Savoy (1663\u20131736). The ensemble was built in the early eighteenth century by the famous Baroque architect, Johann Lucas von Hildebrandt, and comprises the Upper and Lower Belvedere","setOutTime":"2018-08-13","culturalMarks":9,"scenicAreaName":"Heldenplatz","playDays":1},{"imgUrl":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1534211961060&di=ebfef2597b05288b9959668ffed672f6&imgtype=0&src=http://img.youtx.com/guide/201306/17/596/5fdff50ac112a218f50d7d7cdf061945.jpeg","lanscapeMarks":10,"entertainmentMarks":8,"description":"The Belvedere palaces were the summer residence of Prince Eugene of Savoy (1663\u20131736). The ensemble was built in the early eighteenth century by the famous Baroque architect, Johann Lucas von Hildebrandt, and comprises the Upper and Lower Belvedere","setOutTime":"2018-08-14","culturalMarks":8,"scenicAreaName":"Minoritenkirche","playDays":1},{"imgUrl":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1534211961060&di=ebfef2597b05288b9959668ffed672f6&imgtype=0&src=http://img.youtx.com/guide/201306/17/596/5fdff50ac112a218f50d7d7cdf061945.jpeg","lanscapeMarks":9,"entertainmentMarks":2,"description":"The Belvedere palaces were the summer residence of Prince Eugene of Savoy (1663\u20131736). The ensemble was built in the early eighteenth century by the famous Baroque architect, Johann Lucas von Hildebrandt, and comprises the Upper and Lower Belvedere","setOutTime":"2018-08-15","culturalMarks":10,"scenicAreaName":"Neue Burg","playDays":1},{"imgUrl":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1534211961060&di=ebfef2597b05288b9959668ffed672f6&imgtype=0&src=http://img.youtx.com/guide/201306/17/596/5fdff50ac112a218f50d7d7cdf061945.jpeg","lanscapeMarks":10,"entertainmentMarks":1,"description":"The Belvedere palaces were the summer residence of Prince Eugene of Savoy (1663\u20131736). The ensemble was built in the early eighteenth century by the famous Baroque architect, Johann Lucas von Hildebrandt, and comprises the Upper and Lower Belvedere","setOutTime":"2018-08-16","culturalMarks":9,"scenicAreaName":"Maria Theresia Denkmal","playDays":1}]
     * city : Vienna
     * thirdImportant : entertainment
     * days : 3
     * scenicAreaCount : 4
     * planId : 77
     * setOutTime : 2018-08-13
     * firstImportant : lanscape
     * publishStatus : 0
     */
   private String planName;
    private String secondImportant;
    private String city;
    private String thirdImportant;
    private int days;
    private int scenicAreaCount;
    private int planId;
    private String setOutTime;
    private String firstImportant;
    private int publishStatus;
    private String playTimes;

    public void setPlayTimes(String playTimes) {
        this.playTimes = playTimes;
    }

    public String getPlayTimes() {
        return playTimes;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    private List<?> joinUserList;
    private List<PlanScenicAreaListBean> planScenicAreaList;

    public String getSecondImportant() {
        return secondImportant;
    }

    public void setSecondImportant(String secondImportant) {
        this.secondImportant = secondImportant;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getThirdImportant() {
        return thirdImportant;
    }

    public void setThirdImportant(String thirdImportant) {
        this.thirdImportant = thirdImportant;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getScenicAreaCount() {
        return scenicAreaCount;
    }

    public void setScenicAreaCount(int scenicAreaCount) {
        this.scenicAreaCount = scenicAreaCount;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public String getSetOutTime() {
        return setOutTime;
    }

    public void setSetOutTime(String setOutTime) {
        this.setOutTime = setOutTime;
    }

    public String getFirstImportant() {
        return firstImportant;
    }

    public void setFirstImportant(String firstImportant) {
        this.firstImportant = firstImportant;
    }

    public int getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(int publishStatus) {
        this.publishStatus = publishStatus;
    }

    public List<?> getJoinUserList() {
        return joinUserList;
    }

    public void setJoinUserList(List<?> joinUserList) {
        this.joinUserList = joinUserList;
    }

    public List<PlanScenicAreaListBean> getPlanScenicAreaList() {
        return planScenicAreaList;
    }

    public void setPlanScenicAreaList(List<PlanScenicAreaListBean> planScenicAreaList) {
        this.planScenicAreaList = planScenicAreaList;
    }

    public static class PlanScenicAreaListBean {
        /**
         * imgUrl : https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1534211961060&di=ebfef2597b05288b9959668ffed672f6&imgtype=0&src=http://img.youtx.com/guide/201306/17/596/5fdff50ac112a218f50d7d7cdf061945.jpeg
         * lanscapeMarks : 10
         * entertainmentMarks : 9
         * description : The Belvedere palaces were the summer residence of Prince Eugene of Savoy (1663–1736). The ensemble was built in the early eighteenth century by the famous Baroque architect, Johann Lucas von Hildebrandt, and comprises the Upper and Lower Belvedere
         * setOutTime : 2018-08-13
         * culturalMarks : 9
         * scenicAreaName : Heldenplatz
         * playDays : 1
         */
        private String playTimes;
        private String imgUrl;
        private int lanscapeMarks;
        private int entertainmentMarks;
        private String description;
        private String setOutTime;
        private int culturalMarks;
        private String scenicAreaName;
        private int playDays;

        public String getPlayTimes() {
            return playTimes;
        }

        public void setPlayTimes(String playTimes) {
            this.playTimes = playTimes;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public int getLanscapeMarks() {
            return lanscapeMarks;
        }

        public void setLanscapeMarks(int lanscapeMarks) {
            this.lanscapeMarks = lanscapeMarks;
        }

        public int getEntertainmentMarks() {
            return entertainmentMarks;
        }

        public void setEntertainmentMarks(int entertainmentMarks) {
            this.entertainmentMarks = entertainmentMarks;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSetOutTime() {
            return setOutTime;
        }

        public void setSetOutTime(String setOutTime) {
            this.setOutTime = setOutTime;
        }

        public int getCulturalMarks() {
            return culturalMarks;
        }

        public void setCulturalMarks(int culturalMarks) {
            this.culturalMarks = culturalMarks;
        }

        public String getScenicAreaName() {
            return scenicAreaName;
        }

        public void setScenicAreaName(String scenicAreaName) {
            this.scenicAreaName = scenicAreaName;
        }

        public int getPlayDays() {
            return playDays;
        }

        public void setPlayDays(int playDays) {
            this.playDays = playDays;
        }
    }
}
