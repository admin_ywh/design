package com.example.administrator.design.bean;

/**
 * Created by xiaochen on 2017/2/22.
 * 公共配置返回信息
 *
 * @email xiaochenu@gmail.com
 */

public class PublicConfigResponseInfo extends BaseInfo {

    /**
     * host : http://api.zwgt.com
     * file_prefix : http://api.zwgt.com/data/uploads/
     * cur_version : 3.0.0
     * default_key : da6665c14f5b9749
     * service_phone : 0571-81915104
     */

    public String host;
    public String file_prefix;
    public String cur_version;
    public String default_key;
    public String service_phone;
}
