package com.example.administrator.design.ui.main.contacts;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;
import com.example.administrator.design.bean.ScenicSpotBean;

import java.util.List;

public interface AddContact {
    interface view extends BaseView{

        void setDataSpot(List<ScenicSpotBean.ScenicspotBean> scenicspot);

        String getPlanName();

        String getScenicSpotIds();

        String getOutTime();

        String getType();

        void againLogin();

        String getCityId();

        void OnRefreshSuccess();

        void addTripSuccess();
    }
    interface presenter extends BasePresenter{

        void getTripList();

        void addTrop();

        void onRefresh();
    }
}
