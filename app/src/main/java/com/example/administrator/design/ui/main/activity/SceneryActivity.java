package com.example.administrator.design.ui.main.activity;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.bean.RecommendScenicSpotBean;

import butterknife.BindView;
import butterknife.OnClick;

public class SceneryActivity extends BaseActivity {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.rl_top)
    RelativeLayout rlTop;
    @BindView(R.id.iv_pic)
    ImageView ivPic;
    @BindView(R.id.tv_content)
    TextView tvContent;
    private String content,imageUrl;

    @Override
    public int getLayoutId() {
        return R.layout.activity_scenery;
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initViews() {
      content=getIntent().getStringExtra("content");
      imageUrl=getIntent().getStringExtra("imageUrl");
        Glide.with(context).load(imageUrl).into(ivPic);
        tvContent.setText(content);

    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        finish();
    }
}
