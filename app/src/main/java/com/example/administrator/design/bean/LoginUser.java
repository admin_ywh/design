package com.example.administrator.design.bean;

/**
 * Created by xiaochen on 2017/2/23.
 * 用来作为请求体上传
 *
 * @email xiaochenu@gmail.com
 */

public class LoginUser {
    String phone;
    String passwd;

    public LoginUser() {
    }

    public LoginUser(String phone, String passwd) {
        this.phone = phone;
        this.passwd = passwd;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Override
    public String toString() {
        return "LoginUser{" +
                "phone='" + phone + '\'' +
                ", passwd='" + passwd + '\'' +
                '}';
    }
}
