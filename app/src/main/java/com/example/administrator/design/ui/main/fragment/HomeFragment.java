package com.example.administrator.design.ui.main.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseFragment;
import com.example.administrator.design.bean.CityBean;
import com.example.administrator.design.bean.RecommendScenicSpotBean;
import com.example.administrator.design.bean.ScenicSpotBean;
import com.example.administrator.design.ui.login.activity.LoginActivity;
import com.example.administrator.design.ui.main.TripDetailsActivity;
import com.example.administrator.design.ui.main.activity.AddListActivity;
import com.example.administrator.design.ui.main.activity.SceneryActivity;
import com.example.administrator.design.ui.main.activity.SearchActivity;
import com.example.administrator.design.ui.main.activity.SelectCityActivity;
import com.example.administrator.design.ui.main.adapter.ListAdapter;
import com.example.administrator.design.ui.main.adapter.MoreAdapter;
import com.example.administrator.design.ui.main.contacts.HomeContact;
import com.example.administrator.design.ui.main.presenter.HomePresenter;
import com.example.administrator.design.utils.ActivityManager;
import com.orhanobut.hawk.Hawk;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class HomeFragment extends BaseFragment <HomeContact.presenter> implements HomeContact.homeView {
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.rv_city)
    RecyclerView rvCity;
    @BindView(R.id.rl_more)
    RelativeLayout rlMore;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.tv_more)
    TextView tvMore;
    @BindView(R.id.tv_Recommend)
    TextView tvRecommend;
    @BindView(R.id.tv_Recommend_more)
    TextView tvRecommendMore;
    @BindView(R.id.rl_Recommend_more)
    RelativeLayout rlRecommendMore;


    private List<String> list = new ArrayList<>();
    private List< RecommendScenicSpotBean.ScenicspotBean> list_path = new ArrayList<>();
    @BindView(R.id.home_banner)
    Banner homeBanner;

    private List<CityBean.CitysBean> data = new ArrayList<>();
    private MoreAdapter moreAdapter;
    private ListAdapter listAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView() {




        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCity.setLayoutManager(linearLayoutManager);
        moreAdapter = new MoreAdapter(data);
        rvCity.setAdapter(moreAdapter);
        moreAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                startActivity(new Intent(getContext(),AddListActivity.class)
                        .putExtra("cityId",moreAdapter.getItem(position).getCityId()));

            }
        });

        rvList.setNestedScrollingEnabled(false);
        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        listAdapter = new ListAdapter(list_path);
        rvList.setAdapter(listAdapter);
        listAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            if(view.getId()==R.id.iv_pic){
                startActivity(new Intent(getContext(),SceneryActivity.class)
                        .putExtra("imageUrl",listAdapter.getItem(position).getImgUrls())
                        .putExtra("content",listAdapter.getItem(position).getDescription()));
            }
        });
        /**
         * 详情页面
         */
//        listAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
////            @Override
////            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
////                startActivity(new Intent(getContext(), TripDetailsActivity.class)
////                .putExtra("planId",listAdapter.getItem(position).getId()));
////            }
////        });
    }



    @OnClick({R.id.iv_search, R.id.home_banner,R.id.rl_more})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_search:
                startActivity(new Intent(getContext(), SearchActivity.class));
                break;
            case R.id.home_banner:
                break;
            case R.id.rl_more:
                startActivity(new Intent(getContext(), SelectCityActivity.class));
                break;
        }
    }

    @Override
    public String getToken() {
        return Hawk.get("token");
    }

    @Override
    public void setLoadData(List<CityBean.CitysBean> data) {
        moreAdapter.setNewData(data);
    }

    @Override
    public void setDataSpot(List<RecommendScenicSpotBean.ScenicspotBean> scenicspot) {
        if (scenicspot.size()>0){
            listAdapter.setNewData(scenicspot);
        }

        updataBanner(scenicspot);
    }

    @Override
    public void againLogin() {
        startActivity(new Intent(getContext(), LoginActivity.class));
        ActivityManager.getAppInstance().finishAllActivity();
    }

    private void updataBanner(List<RecommendScenicSpotBean.ScenicspotBean> scenicspot) {
        for(int i=0;i<scenicspot.size();i++){
            list.add(scenicspot.get(i).getImgUrls());
        }
        homeBanner.setImageLoader(new GlideImageLoader());
        //设置图片集合
        homeBanner.setImages(list);
        //设置轮播时间
        homeBanner.setDelayTime(3000);
        //设置指示器位置（当banner模式中有指示器时）
        homeBanner.setIndicatorGravity(BannerConfig.RIGHT);
        //设置banner动画效果
//        homeBanner.setBannerAnimation(Transformer.CubeOut);
        //设置点击事件
        homeBanner.setOnBannerClickListener(position -> {
            homeBanner.setIndicatorGravity(BannerConfig.CENTER);
        });
        //banner设置方法全部调用完毕时最后调用
        homeBanner.start();
        homeBanner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                startActivity(new Intent(getContext(),SceneryActivity.class)
                .putExtra("imageUrl",scenicspot.get(position).getImgUrls())
                .putExtra("content",scenicspot.get(position).getDescription()));
            }
        });
    }


    public class GlideImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            //具体方法内容自己去选择，次方法是为了减少banner过多的依赖第三方包，所以将这个权限开放给使用者去选择
            Glide.with(getContext())
                    .load(path)
                    .into(imageView);

        }
    }

    @Override
    public HomeContact.presenter initPresenter() {
        return new HomePresenter(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getAppRecommendScenicSpot();
        presenter.getCityList();

    }
}
