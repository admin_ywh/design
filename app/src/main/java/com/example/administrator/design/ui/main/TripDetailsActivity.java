package com.example.administrator.design.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.bean.TripDetailsBean;
import com.example.administrator.design.ui.login.activity.LoginActivity;
import com.example.administrator.design.ui.main.activity.SceneryActivity;
import com.example.administrator.design.ui.main.adapter.PersonListAdapter;
import com.example.administrator.design.ui.main.adapter.TripDetailsAdapter;
import com.example.administrator.design.ui.main.contacts.TripDetailsContact;
import com.example.administrator.design.ui.main.presenter.TripDetailsPresenter;
import com.example.administrator.design.utils.ActivityManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TripDetailsActivity extends BaseActivity<TripDetailsContact.presenter> implements TripDetailsContact.view {


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rv_personal)
    RecyclerView rvPersonal;
    @BindView(R.id.rv_details)
    RecyclerView rvDetails;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.time)
    TextView times;
    @BindView(R.id.send)
    TextView send;
    @BindView(R.id.sum)
    TextView sum;
    @BindView(R.id.days)
    TextView days;
    @BindView(R.id.city)
    TextView city;
    @BindView(R.id.first)
    TextView first;
    @BindView(R.id.second)
    TextView second;
    @BindView(R.id.trid)
    TextView trid;
    @BindView(R.id.sign_sum)
    TextView signSum;
    @BindView(R.id.times)
    TextView Tvtimes;
    private PersonListAdapter adapter;
    private TripDetailsAdapter tripDetailsAdapter;
    private List<TripDetailsBean.PlanScenicAreaListBean> data = new ArrayList<>();
    private List<TripDetailsBean.PlanScenicAreaListBean> time = new ArrayList<>();
    private int planId;


    @Override
    public int getLayoutId() {
        return R.layout.activity_trip_details;
    }

    @Override
    public TripDetailsContact.presenter initPresenter() {
        return new TripDetailsPresenter(this);
    }

    @Override
    protected void initViews() {
        planId = getIntent().getIntExtra("planId", -1);
//        if (Ttitle != null) {
//            title.setText("Name of travel: " + Ttitle);
//            times.setText("Name of time: " + Triptime);
//        }
        rvDetails.setNestedScrollingEnabled(false);
        rvDetails.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvPersonal.setLayoutManager(linearLayoutManager);
        adapter = new PersonListAdapter(data);
        rvPersonal.setAdapter(adapter);

        rvDetails.setLayoutManager(new LinearLayoutManager(this));
        tripDetailsAdapter = new TripDetailsAdapter(time);
        rvDetails.setAdapter(tripDetailsAdapter);

        tripDetailsAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            if (view.getId() == R.id.iv_pic) {
                startActivity(new Intent(TripDetailsActivity.this, SceneryActivity.class)
                        .putExtra("imageUrl", tripDetailsAdapter.getItem(position).getImgUrl())
                        .putExtra("content", tripDetailsAdapter.getItem(position).getDescription()));
            }
        });
    }


    @Override
    public String getPlanId() {
        return planId + "";
    }

    @Override
    public void LoadData(TripDetailsBean data) {
        sum.setText("Name of sum: " + data.getPlanScenicAreaList().size());
        first.setText("firstImportant：" + data.getFirstImportant());
        second.setText("firstImportant：" + data.getThirdImportant());
        trid.setText("firstImportant：" + data.getThirdImportant());
        title.setText("Name of travel: " + data.getPlanName());
        days.setText("days：" + data.getDays());
        times.setText("Name of time: " + data.getSetOutTime());
        city.setText("City：" + data.getCity());
        signSum.setText("Number of scenic spots：" + data.getScenicAreaCount());
        Tvtimes.setText("playTimes："+data.getPlayTimes()+"hours");
        adapter.setNewData(data.getPlanScenicAreaList());
        tripDetailsAdapter.setNewData(data.getPlanScenicAreaList());
        updata(data);
    }

    private void updata(TripDetailsBean data) {
        if (data.getPublishStatus() == 0) {
            send.setText("Release");
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.pushTrip();

                }
            });
        } else if (data.getPublishStatus() == 1) {
            send.setText("Revocation");
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.RevertpushTrip();
                }
            });
        } else {
            send.setText("cancel");
        }
    }

    @Override
    public void againLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        ActivityManager.getAppInstance().finishAllActivity();
    }

    @Override
    public void pushSuccess() {
        presenter.getTravelPlanDetail();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getTravelPlanDetail();
    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;

        }
    }

}
