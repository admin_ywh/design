package com.example.administrator.design.ui.main.contacts;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;

public interface CreateTripContact {
    interface view extends BaseView{

        String getPlanName();

        String getOutTime();

        String getDays();

        String getplayTimes();

        String getCityId();


        void againLogin();

        int getfirstImportant();

        int getsecondImportant();

        int getthirdImportant();

        void success(int planId);
    }
    interface presenter extends BasePresenter{

        void crateTrip();
    }
}
