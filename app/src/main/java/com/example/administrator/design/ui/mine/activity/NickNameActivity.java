package com.example.administrator.design.ui.mine.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.bean.UpdateUserBean;
import com.example.administrator.design.ui.mine.contacts.Nickcontact;
import com.example.administrator.design.ui.mine.presenter.NickPresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class NickNameActivity extends BaseActivity<Nickcontact.presenter> implements Nickcontact.view {


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.et_userName)
    EditText etUserName;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    @Override
    public int getLayoutId() {
        return R.layout.activity_nick_name;
    }

    @Override
    public Nickcontact.presenter initPresenter() {
        return new NickPresenter(this);
    }

    @Override
    protected void initViews() {

    }

    @OnClick({R.id.iv_back, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                break;
            case R.id.btn_submit:
                presenter.setUserName();
                break;
        }
    }


    @Override
    public String getNickName() {
        return etUserName.getText().toString().trim();
    }

    @Override
    public void Successs(UpdateUserBean data) {
        //数据是使用Intent返回
        Intent intent = new Intent();
        //把返回数据存入Intent
        intent.putExtra("NickName", etUserName.getText().toString().toString());
        //设置返回数据
        this.setResult(1, intent);
        //关闭Activity
        this.finish();
    }
}
