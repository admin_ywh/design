package com.example.administrator.design.ui.main.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.administrator.design.R;
import com.example.administrator.design.bean.TripListBean;

import java.util.List;

public class TripListAdapter extends BaseQuickAdapter<TripListBean.TravelPlanListBean,BaseViewHolder> {
    public TripListAdapter(@Nullable List<TripListBean.TravelPlanListBean> data) {
        super(R.layout.layout_trip_list, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TripListBean.TravelPlanListBean item) {
        ImageView ivPic=helper.getView(R.id.iv_pic);
        Glide.with(mContext).load(item.getImgUrl()).into(ivPic);
        helper.addOnClickListener(R.id.rl_add);
        helper.setText(R.id.tv_title,item.getPlanName());
        helper.setText(R.id.tv_time,"Time:"+item.getTime());
    }
}
