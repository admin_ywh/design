package com.example.administrator.design.base.mvpbase.baseImpl;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.android.tu.loadingdialog.LoadingDailog;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;
import com.example.administrator.design.utils.ActivityManager;
import com.example.administrator.design.utils.StatusBarCompat;


import java.util.Set;

import butterknife.ButterKnife;


/*
 * 项目名:    BaseLib
 * 包名       com.zhon.baselib.mvpbase.baseImpl
 * 文件名:    BaseActivity
 * 创建者:    ZJB
 * 创建时间:  2017/9/7 on 14:17
 * 描述:     TODO 基类Activity
 */
public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity
        implements BaseView {
    private LoadingDailog.Builder loadBuilder;
    private  LoadingDailog dialog;
    protected P presenter;
    public Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        context = this;
        SetStatusBarColor();
        setContentView(getLayoutId());
        ButterKnife.bind(this);


        getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        initViews();
        ActivityManager.getAppInstance().addActivity(this);//将当前activity添加进入管理栈
         initDialog();
        presenter = initPresenter();
    }

    private void initDialog() {
        loadBuilder=new LoadingDailog.Builder(this)
                .setMessage("加载中")
                .setCancelable(true)
                .setCancelOutside(true);
        dialog=loadBuilder.create();
    }



    /**
     * 着色状态栏（4.4以上系统有效）
     */
    protected void SetStatusBarColor(){
        StatusBarCompat.setStatusBarColor(this, ContextCompat.getColor(this, R.color.main_color));
    }
    /**
     * 着色状态栏（4.4以上系统有效）
     */
    protected void SetStatusBarColor(int color){
        StatusBarCompat.setStatusBarColor(this,color);
    }
    /**
     * 沉浸状态栏（4.4以上系统有效）
     */
    protected void SetTranslanteBar(){
        StatusBarCompat.translucentStatusBar(this);
    }
    @Override
    protected void onDestroy() {
        ActivityManager.getAppInstance().removeActivity(this);//将当前activity移除管理栈
        if (presenter != null) {
            presenter.detach();//在presenter中解绑释放view
            presenter = null;
        }
        super.onDestroy();
    }

    /**
     * 初始化布局
     * @return
     */
    public abstract int getLayoutId();
    /**
     * 在子类中初始化对应的presenter
     *
     * @return 相应的presenter
     */
    public abstract P initPresenter();

    /**
     * 初始化view
     */
    protected abstract void initViews();


    @Override
    public void dismissLoadingDialog() {
       dialog.dismiss();
    }

    @Override
    public void showLoadingDialog() {
        dialog.show();
    }

}
