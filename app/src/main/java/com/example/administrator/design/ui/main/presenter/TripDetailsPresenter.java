package com.example.administrator.design.ui.main.presenter;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseBean;
import com.example.administrator.design.base.mvpbase.baseImpl.BasePresenterImpl;
import com.example.administrator.design.bean.TestBean;
import com.example.administrator.design.bean.TripDetailsBean;
import com.example.administrator.design.retroft.Api;
import com.example.administrator.design.retroft.ExceptionHelper;
import com.example.administrator.design.ui.main.contacts.TripDetailsContact;
import com.orhanobut.hawk.Hawk;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TripDetailsPresenter extends BasePresenterImpl<TripDetailsContact.view>implements TripDetailsContact.presenter {

    public TripDetailsPresenter(TripDetailsContact.view view) {
        super(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void getTravelPlanDetail() {
        String token=Hawk.get("token");
        Api.getInstance().getTravelPlanDetail(token, view.getPlanId())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoadingDialog();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(detailsBeanBaseBean -> {
                    view.dismissLoadingDialog();
                    //处理服务器返回的数据
                    if (detailsBeanBaseBean.getCode().equals("0"))
                    {
                       view.LoadData(detailsBeanBaseBean.getData());
                        //处理服务器返回的错误信息
                    } else if(detailsBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }{
                        Log.e("Throwable", detailsBeanBaseBean.getMsg());
                        ToastUtils.showShort(detailsBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    view.dismissLoadingDialog();
                    ExceptionHelper.handleException(throwable);
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void pushTrip() {
        Api.getInstance().publishTravelPlan(Hawk.get("token"), view.getPlanId())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoadingDialog();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(detailsBeanBaseBean -> {
                    view.dismissLoadingDialog();
                    //处理服务器返回的数据
                    if (detailsBeanBaseBean.getCode().equals("0"))
                    {
                        view.pushSuccess();
                        ToastUtils.showShort(detailsBeanBaseBean.getMsg());
                        //处理服务器返回的错误信息
                    } else if(detailsBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }{
                        Log.e("Throwable", detailsBeanBaseBean.getMsg());
                        ToastUtils.showShort(detailsBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    view.dismissLoadingDialog();
                    ExceptionHelper.handleException(throwable);
                });
    }

    /**
     *  取消行程
     */
    @SuppressLint("CheckResult")
    @Override
    public void RevertpushTrip() {
        Api.getInstance().cancelTravelPlan(Hawk.get("token"), view.getPlanId())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoadingDialog();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(detailsBeanBaseBean -> {
                    view.dismissLoadingDialog();
                    //处理服务器返回的数据
                    if (detailsBeanBaseBean.getCode().equals("0"))
                    {
                        view.pushSuccess();
                        ToastUtils.showShort(detailsBeanBaseBean.getMsg());
                        //处理服务器返回的错误信息
                    } else if(detailsBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }{
                        Log.e("Throwable", detailsBeanBaseBean.getMsg());
                        ToastUtils.showShort(detailsBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    view.dismissLoadingDialog();
                    ExceptionHelper.handleException(throwable);
                });
    }
}
