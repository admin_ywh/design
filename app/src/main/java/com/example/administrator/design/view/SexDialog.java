package com.example.administrator.design.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.administrator.design.R;

public class SexDialog extends Dialog implements RadioGroup.OnCheckedChangeListener {
    private OnCheckedChangeListener listener;
    private Context mcontext;
    View view;
    private RadioGroup rpSex;
    private RadioButton rbMan, rbWomen;

    public SexDialog(@NonNull Context context) {
        super(context);
        this.mcontext = context;
        initView();
        initListener();

    }

    public SexDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mcontext = context;
        initView();
        initListener();

    }

    private void initListener() {
        rpSex.setOnCheckedChangeListener(this);
    }


    private void initView() {
        view = LayoutInflater.from(mcontext).inflate(R.layout.layout_sex_dialog, null);
        setContentView(view);
        rpSex = view.findViewById(R.id.rp_sex);
        rbMan = view.findViewById(R.id.ck_man);
        rbWomen = view.findViewById(R.id.ck_womem);
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checked) {
        if (checked == R.id.ck_man) {
          if (listener!=null){
              listener.OnCheckedChangeListener(0);
          }
        } else if (checked == R.id.ck_womem) {
            if (listener!=null){
                listener.OnCheckedChangeListener(1);
            }
        }
    }

    public interface OnCheckedChangeListener {
        void OnCheckedChangeListener(int position);
    }
    public void setOnCheckedChangeListener(OnCheckedChangeListener listener){
        this.listener=listener;

    }

}
