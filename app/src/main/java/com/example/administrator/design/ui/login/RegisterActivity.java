package com.example.administrator.design.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseBean;
import com.example.administrator.design.bean.TestBean;
import com.example.administrator.design.ui.login.activity.LoginActivity;
import com.example.administrator.design.ui.login.contact.RegisterContact;
import com.example.administrator.design.ui.login.presenter.RegisterPresenter;
import com.example.administrator.design.ui.main.activity.SelectCityActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity<RegisterContact.RegisterPresenter> implements RegisterContact.RegisterView {


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.again_password)
    EditText againPassword;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.userId)
    EditText userId;

    @Override
    public int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    public RegisterContact.RegisterPresenter initPresenter() {
        return new RegisterPresenter(this);
    }

    @Override
    protected void initViews() {

    }


    @OnClick({R.id.iv_back, R.id.btn_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_register:
                presenter.register();
                break;
        }
    }

    @Override
    public String getPhone() {
        return username.getText().toString().trim();
    }

    @Override
    public String getPsd() {
        return password.getText().toString().trim();
    }

    @Override
    public String getSubmitPsd() {
        return againPassword.getText().toString().trim();
    }

    @Override
    public String getUserName() {
        return userId.getText().toString().trim();
    }

    @Override
    public void success(BaseBean<TestBean> storiesBeen) {
        startActivity(new Intent(this, LoginActivity.class));
        this.finish();
    }

}
