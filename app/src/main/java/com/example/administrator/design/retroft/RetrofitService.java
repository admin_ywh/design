package com.example.administrator.design.retroft;


import com.example.administrator.design.base.mvpbase.baseImpl.BaseBean;
import com.example.administrator.design.bean.AddTripBean;
import com.example.administrator.design.bean.CityBean;
import com.example.administrator.design.bean.CreateBean;
import com.example.administrator.design.bean.GuestInfo;
import com.example.administrator.design.bean.RecommendScenicSpotBean;
import com.example.administrator.design.bean.ScenicSpotBean;
import com.example.administrator.design.bean.TestBean;
import com.example.administrator.design.bean.TripDetailsBean;
import com.example.administrator.design.bean.TripListBean;
import com.example.administrator.design.bean.UpdateUserBean;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface RetrofitService {
    String wendangUrl = "http://export.eolinker.com/html/653a40de0d3d8c8e703f015ae6f8bc53/1532855697.html";
    String BASE_URL = "http://111.230.203.152:8080/api/";
//    String BASE_URL = "http:\\/\\/api.zwgt.com";

    /**
     * 登录接口
     *
     * @return
     */
    @GET("userLogin")
    Observable<BaseBean<TestBean>> login(@Query("phone") String phone,
                                         @Query("password") String password);


    /*    @POST("/app-login")
        Observable<LoginResponseInfo> login(@HeaderMap HashMap<String, String> map,
                                                      @Body LoginUser lu);*/
    @GET("userRegister")
    Observable<BaseBean<TestBean>> register(@Query("phone") String phone,
                                            @Query("password") String password,
                                            @Query("nickName") String nickName);

    /**
     * 查询所有列表
     */
    @GET("/{path}")
    Observable<BaseBean<GuestInfo>> getGuestList(@Path("path") String path,
                                                 @HeaderMap HashMap<String, String> map);

    /**
     * 提交推荐城市id
     *
     * @param token
     * @param cityIds
     * @return
     */
    @GET("recommendCity")
    Observable<BaseBean<TestBean>> submitCityId(@Query("token") String token,
                                                @Query("cityIds") String cityIds);

    /**
     * 城市列表
     *
     * @param token
     * @return
     */
    @GET("cityList")
    Observable<BaseBean<CityBean>> cityList(@Query("token") String token);

    /**
     * app首页数据——推荐景点
     *
     * @param token
     * @return
     */
    @GET("getScenicSpotList")
    Observable<BaseBean<ScenicSpotBean>> ScenicSpot(@Query("token") String token,
                                                    @Query("type") String type,
                                                    @Query("cityId")String cityId);

    /**
     * 行程列表
     *
     * @param token
     * @return
     */
    @GET("getTravelPlanList")
    Observable<BaseBean<TripListBean>> tripList(@Query("token") String token,
                                                @Query("dataFlag") String dataFlag);

    /**
     * 获取推荐城市
     *
     * @param token
     * @return
     */
    @GET("getRecommendCitys")
    Observable<BaseBean<CityBean>> getMainCityList(@Query("token") String token
    );

    /**
     * 首页推荐景点
     *
     * @param token
     * @return
     */
    @GET("appRecommendScenicSpot")
    Observable<BaseBean<RecommendScenicSpotBean>> appRecommendScenicSpot(@Query("token") String token
    );

    /**
     * 修改用户信息
     *
     * @param token
     * @param nickName
     * @param headImg
     * @param sex
     * @return
     */
    @GET("updateUser")
    Observable<BaseBean<UpdateUserBean>> updateUser(@Query("token") String token,
                                                    @Query("nickName") String nickName,
                                                    @Query("headImg") String headImg,
                                                    @Query("sex") String sex);

    /**
     * 行程计划详情页面数据
     *
     * @return
     */
    @GET("getTravelPlanDetail")
    Observable<BaseBean<TripDetailsBean>> getTravelPlanDetail(@Query("token") String token,
                                                              @Query("planId") String planId);

    /**
     * 行程加入接口
     *
     * @param token
     * @param planId
     * @param userId
     * @return
     */
    @GET("joinTravelPlan")
    Observable<BaseBean<TripDetailsBean>> joinTrip(@Query("token") String token,
                                                   @Query("planId") String planId,
                                                   @Query("userId") String userId);

    /**
     * 行程添加景点接口
     *
     * @param token
     * @param scenicSpotId
     * @param planId
     * @param days
     * @param planContent
     * @param maxJoinCount
     * @param cost
     * @param setOutTime
     * @param distance
     * @return
     */
    @GET("addTravelPlanScenicArea")
    Observable<BaseBean<TripDetailsBean>> addTravelPlanScenicArea(@Query("token") String token,
                                                                  @Query("scenicSpotId") String scenicSpotId,
                                                                  @Query("planId") String planId,
                                                                  @Query("days") String days,
                                                                  @Query("planContent") String planContent,
                                                                  @Query("maxJoinCount") String maxJoinCount,
                                                                  @Query("cost") String cost,
                                                                  @Query("setOutTime") String setOutTime,
                                                                  @Query("distance") String distance
    );

    /**
     * 添加行程接口
     *
     * @param token
     * @param planName
     * @param scenicSpotIds 选择的景区ID
     * @param setOutTime
     * @return
     */
    @GET("addTravelPlan")
    Observable<BaseBean<AddTripBean>> addTrip(@Query("token") String token,
                                              @Query("planName") String planName,
                                              @Query("scenicSpotIds") String scenicSpotIds,
                                              @Query("setOutTime") String setOutTime);
    /**
     *  添加行程接口
     * @param token
     * @param planName
     * @param setOutTime
     * @param days
     * @param playTimes
     * @param cityId
     * @param firstImportant
     * @param secondImportant
     * @param thirdImportant
     * @return
     */
    @GET("addTravelPlan")
    Observable<BaseBean<CreateBean>> addTrip1(@Query("token") String token,
                                              @Query("planName") String planName,
                                              @Query("setOutTime") String setOutTime,
                                              @Query("days")String days,
                                              @Query("playTimes")String playTimes,
                                              @Query("cityId")String cityId,
                                              @Query("firstImportant")int firstImportant,
                                              @Query("secondImportant")int secondImportant,
                                              @Query("thirdImportant")int thirdImportant);
    /**
     * 取消行程加入
     *
     * @param token
     * @param planId
     * @param userId
     * @return
     */
    @GET("cancelJoinTravelPlan")
    Observable<BaseBean<AddTripBean>> cancelJoinTravelPlan(@Query("token") String token,
                                                           @Query("planId") String planId,
                                                           @Query("userId") String userId);

    /**
     *   发布行程
     * @param token
     * @param planId
     * @return
     */
    @GET("publishTravelPlan")
    Observable<BaseBean<TestBean>> publishTravelPlan(@Query("token") String token,
                                                     @Query("planId") String planId);

    /**
     *  取消行程
     * @param token
     * @param planId
     * @return
     */
    @GET("cancelTravelPlan")
    Observable<BaseBean<TestBean>> cancelTravelPlan(@Query("token") String token,
                                                     @Query("planId") String planId);
}

