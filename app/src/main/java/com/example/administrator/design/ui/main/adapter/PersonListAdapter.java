package com.example.administrator.design.ui.main.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.administrator.design.R;
import com.example.administrator.design.bean.TripDetailsBean;

import java.util.List;

public class PersonListAdapter extends BaseQuickAdapter<TripDetailsBean.PlanScenicAreaListBean,BaseViewHolder> {
    public PersonListAdapter(@Nullable List<TripDetailsBean.PlanScenicAreaListBean> data) {
        super(R.layout.layout_more_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TripDetailsBean.PlanScenicAreaListBean item) {
//        helper.setText(R.id.tv_city,item.get));
    }
}
