package com.example.administrator.design.ui.main.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.bean.CityBean;
import com.example.administrator.design.ui.main.adapter.RadioCityAdapter;
import com.example.administrator.design.ui.main.contacts.SelectCityContact;
import com.example.administrator.design.ui.main.listener.OnItemClickLitener;
import com.example.administrator.design.ui.main.presenter.SelectCityPresenter;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class AddCityActivity extends BaseActivity<SelectCityContact.presenter>
        implements SelectCityContact.homeView{


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.rl_top)
    RelativeLayout rlTop;
    @BindView(R.id.city_list)
    RecyclerView cityList;
    @BindView(R.id.tv_save)
    TextView tvSave;
    private String cityNameStr,cityId;
    private RadioCityAdapter cityAdapter;
    private List<CityBean.CitysBean> data = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_select_city;
    }

    @Override
    public SelectCityContact.presenter initPresenter() {
        return new SelectCityPresenter(this);
    }


    @Override
    protected void initViews() {
        cityList.setLayoutManager(new LinearLayoutManager(this));
        cityAdapter = new RadioCityAdapter(data);
        cityList.setAdapter(cityAdapter);
       cityAdapter.setOnItemClickLitener(new OnItemClickLitener() {
           @Override
           public void onItemClick(View view, int position, String name, String cityName) {
               cityAdapter.setSelection(position);
               Toast.makeText(context, ""+cityNameStr, Toast.LENGTH_SHORT).show();
               cityNameStr=name.toString().trim();
               cityId=cityName.toString().trim();
           }

           @Override
           public void onItemLongClick(View view, int position) {

           }
       });
    }


    @Override
    public String getToken() {
        return Hawk.get("token");
    }

    @Override
    public void LoadData(CityBean bean) {
        data.clear();
        for (int i = 0; i < bean.getCitys().size(); i++) {
            data.add(bean.getCitys().get(i));
        }
        cityAdapter.notifyDataSetChanged();
    }

    @Override
    public void success() {
//        startActivity(new Intent(this,MainActivity.class));
//        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.city();
    }



    @OnClick({R.id.iv_back, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_save:
                cityAdapter.notifyDataSetChanged();
                //数据是使用Intent返回
                Intent intent = new Intent();
                //把返回数据存入Intent
                intent.putExtra("cityName", cityNameStr);
                intent.putExtra("cityId",cityId);
                //设置返回数据
                this.setResult(0, intent);
                //关闭Activity
                this.finish();
                break;
        }
    }

}