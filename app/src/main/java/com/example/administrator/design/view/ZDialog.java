package com.example.administrator.design.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.administrator.design.R;
import com.lcodecore.tkrefreshlayout.utils.DensityUtil;

public class ZDialog extends Dialog {
    private Context mcontext;
    View view;
    private TextView tvCancel, tvSubmit, tvTitle;
    private String cancel, msgTitle, submit;
    private OnclickListener listener;
    public ZDialog(@NonNull Context context) {
        super(context);
        this.mcontext = context;
        initView();
        initListener();
    }

    public ZDialog(@NonNull Context context, String cancel, String msgTitle, String submit, int themeResId) {
        super(context, themeResId);
        this.mcontext = context;
        this.msgTitle = msgTitle;
        this.cancel = cancel;
        this.submit = submit;
        initView();
        initListener();

    }

    private void initListener() {

    }


    private void initView() {
        view = LayoutInflater.from(mcontext).inflate(R.layout.layout_zdialog, null);
        setContentView(view);
        tvCancel = view.findViewById(R.id.tv_cancel);
        tvSubmit = view.findViewById(R.id.tv_submit);
        tvTitle = view.findViewById(R.id.tv_title);
        tvCancel.setText(cancel);
        tvTitle.setText(msgTitle);
        tvSubmit.setText(submit);
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;         // 屏幕宽度（像素）
        int height = dm.heightPixels;
        this.getWindow().setLayout(width*4/5, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(listener!=null){
                   listener.OnClickListener();
                   dismiss();
               }
            }
        });
    }
    public interface OnclickListener{
        void OnClickListener();
    }
    public void setOnclickListener(OnclickListener listener){
        this.listener=listener;
    }
}
