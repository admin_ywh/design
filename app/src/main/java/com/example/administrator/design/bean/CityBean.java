package com.example.administrator.design.bean;

import java.io.Serializable;
import java.util.List;

public class CityBean implements Serializable{


    private List<CitysBean> citys;

    public List<CitysBean> getCitys() {
        return citys;
    }

    public void setCitys(List<CitysBean> citys) {
        this.citys = citys;
    }

    public static class CitysBean implements Serializable{
        /**
         * cityName : 北京城区
         * cityId : 2
         */

        private String cityName;
        private String cityId;

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }
    }
}
