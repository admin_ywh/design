package com.example.administrator.design.ui.main.contacts;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;
import com.example.administrator.design.bean.CityBean;

import java.util.List;

public interface SelectCityContact {
    interface homeView extends BaseView{

        String getToken();

        void LoadData(CityBean data);

        void success();

    }
    interface presenter extends BasePresenter{

        void city();

        void submitCityID(List<CityBean.CitysBean> list);
    }
}
