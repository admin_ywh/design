package com.example.administrator.design.ui.main.contacts;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;
import com.example.administrator.design.bean.TripListBean;

import java.util.List;

public interface TripListContacts {
    interface view extends BaseView{

        String getToken();


        void LoadData(List<TripListBean.TravelPlanListBean> travelPlanList);

        String getDataFlag();

        void Success();

        void finishRefresh();

        void CancelSuccess();

        void againLogin();
    }
    interface presenter extends BasePresenter{

        void tripList();

        void joinTrip(int planId);

        void onRefresh();

        void NojoinTrip(int planId);

        void onRefreshMy();
    }
}
