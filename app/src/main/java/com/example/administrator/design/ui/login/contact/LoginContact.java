package com.example.administrator.design.ui.login.contact;


import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;
import com.example.administrator.design.bean.TestBean;

/*
 * 项目名:    BaseFrame
 * 包名       com.zhon.frame.mvp.login.contact
 * 文件名:    LoginContact
 * 创建者:    ZJB
 * 创建时间:  2017/9/7 on 11:13
 * 描述:     TODO  接口
 */
public interface LoginContact {

    interface view extends BaseView {
        void getLoadData(TestBean testBean);

        String getUserName();

        String getPsd();
        /**
         * 设置数据
         *
         * @param dataList
         */

    }

    interface presenter extends BasePresenter {
        void login();
        /**
         * 获取数据
         */

    }
}
