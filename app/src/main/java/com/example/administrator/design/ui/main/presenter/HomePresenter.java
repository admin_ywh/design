package com.example.administrator.design.ui.main.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.base.mvpbase.baseImpl.BasePresenterImpl;
import com.example.administrator.design.bean.TaskConstants;
import com.example.administrator.design.retroft.Api;
import com.example.administrator.design.retroft.ExceptionHelper;
import com.example.administrator.design.ui.main.contacts.HomeContact;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class HomePresenter extends BasePresenterImpl<HomeContact.homeView>
        implements HomeContact.presenter {

    public HomePresenter(HomeContact.homeView view) {
        super(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void getAppRecommendScenicSpot() {
        Api.getInstance().appRecommendScenicSpot(view.getToken())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoadingDialog();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(secniBean -> {
                    view.dismissLoadingDialog();
                    //处理服务器返回的数据
                    if (secniBean.getCode().equals("0"))
                    {
                        //处理服务器返回的错误信息
                        view.setDataSpot(secniBean.getData().getScenicspot());
                    } else  if(secniBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }else {
                        Log.e("Throwable", secniBean.getMsg());
                        ToastUtils.showShort(secniBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    view.dismissLoadingDialog();
                    ExceptionHelper.handleException(throwable);
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void getCityList() {
        Api.getInstance().getMainCityList(view.getToken())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cityBeanBaseBean -> {
                    //处理服务器返回的数据
                    if (cityBeanBaseBean.getCode().equals("0"))
                    {
                        view.setLoadData(cityBeanBaseBean.getData().getCitys());
                        //处理服务器返回的错误信息
                    } else  if(cityBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }else {
                        Log.e("Throwable", cityBeanBaseBean.getMsg());
                        ToastUtils.showShort(cityBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    ExceptionHelper.handleException(throwable);
                });
    }
}