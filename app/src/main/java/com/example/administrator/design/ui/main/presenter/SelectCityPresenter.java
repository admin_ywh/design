package com.example.administrator.design.ui.main.presenter;

import android.annotation.SuppressLint;
import android.provider.Settings;
import android.util.Log;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.base.mvpbase.baseImpl.BasePresenterImpl;
import com.example.administrator.design.bean.CityBean;
import com.example.administrator.design.retroft.Api;
import com.example.administrator.design.retroft.ExceptionHelper;
import com.example.administrator.design.ui.main.contacts.SelectCityContact;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SelectCityPresenter extends BasePresenterImpl<SelectCityContact.homeView>
        implements SelectCityContact.presenter {
   private String cityId="";
    public SelectCityPresenter(SelectCityContact.homeView view) {
        super(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void city() {
        Api.getInstance().cityList(view.getToken())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(secniBean -> {
                    //处理服务器返回的数据
                    if (secniBean.getCode().equals("0"))
                    {
                        //处理服务器返回的错误信息
                      view.LoadData(secniBean.getData());
                    } else {
                        Log.e("Throwable", secniBean.getMsg());
                        ToastUtils.showShort(secniBean.getMsg());
                    }
                    //处理网络异常
                }, throwable -> {
                    ExceptionHelper.handleException(throwable);
                });
    }

    /**
     *   提交自己喜欢的城市ID
     * @param list
     */
    @SuppressLint("CheckResult")
    @Override
    public void submitCityID(List<CityBean.CitysBean> list) {

        for(int i=0;i<list.size();i++){
        cityId=cityId+list.get(i).getCityId()+",";
        if(i==(list.size()-1)){
            cityId=cityId.substring(0,cityId.length()-1);
        }
        }

        Api.getInstance().submitCityId(view.getToken(),cityId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(this::addDisposable)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(secniBean -> {
                    //处理服务器返回的数据
                    if (secniBean.getCode().equals("0"))
                    {
                        //处理服务器返回的错误信息
                        view.success();
                      ToastUtils.showShort(secniBean.getMsg());
                    } else {
                        ToastUtils.showShort(secniBean.getMsg());
                    }
                    //处理网络异常
                }, ExceptionHelper::handleException);
    }


}