package com.example.administrator.design.bean;

import java.io.Serializable;
import java.util.List;

public class RecommendScenicSpotBean {

    private List<ScenicspotBean> scenicspot;

    public List<ScenicspotBean> getScenicspot() {
        return scenicspot;
    }

    public void setScenicspot(List<ScenicspotBean> scenicspot) {
        this.scenicspot = scenicspot;
    }

    public static class ScenicspotBean implements Serializable{
        /**
         * cost : 100元/人
         * imgUrls : http://www.baiyunshan.com.cn/publicfiles///business/htmlfiles/mcg/cmsmedia/image/img8827original.jpg,http://www.baiyunshan.com.cn/publicfiles///business/htmlfiles/mxl/cmsmedia/image/img4708original.jpg
         * description : 白云山，位于广东省广州市白云区，为南粤名山之一，自古就有“羊城第一秀”之称。山体相当宽阔，由30多座山峰组成，为广东最高峰九连山的支脉。面积20.98平方公里，主峰摩星岭高382米。
         * positionLngLat : 113.2888063, 23.1546538
         * visitCount : 10000
         * websiteUrl : http://www.baiyunshan.com.cn/
         * name : 白云山
         * characteristicDesc : 白云山已建设成为的国家级风景名胜区和旅游景区。白云山风景区从南至北共有7个游览区 [7] ，依次是：麓湖景区、三台岭景区、鸣春谷景区、摩星岭景区、明珠楼景区、飞鹅岭景区及荷依岭景区，区内有三个全国之最的景点，分别是：全国最大的园林式花园——云台花园；全国最大的天然式鸟笼——鸣春谷；全国最大的主题式雕塑专类公园——雕塑公园。
         * location : 广东省广州市白云区
         * viewCount : 10000
         * id : 1
         * travelGuid : 麓湖公园位于广州市北面，白云山风景名胜区南端，占地面积205万平方米，其中麓湖的水体面积21万平方米。

         公园经过多年建设，逐步成为一个以湖光山色著称的城市大型山水园林。“一山环秀水，半岭隐涛声”正是公园的生动写照，园内林木苍翠，鸟语花香，亭榭桥廊点缀其中，是一片城市中的绿洲。园内的景点有聚芳园、儿童乐园、星海园、白云仙馆，饮食休闲娱乐的地方有鹿鸣酒家、畔山明珠、游艇部等，还有一个18洞国际标准的高尔夫球场。

         无论你专程畅游还是休闲娱乐，尽可令你饱览岭南园林秀色，心旷神怡，流连忘返。
         * playDays : 1
         */

        private String cost;
        private String imgUrls;
        private String description;
        private String positionLngLat;
        private int visitCount;
        private String websiteUrl;
        private String name;
        private String characteristicDesc;
        private String location;
        private int viewCount;
        private int id;
        private String travelGuid;
        private int playDays;

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getImgUrls() {
            return imgUrls;
        }

        public void setImgUrls(String imgUrls) {
            this.imgUrls = imgUrls;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPositionLngLat() {
            return positionLngLat;
        }

        public void setPositionLngLat(String positionLngLat) {
            this.positionLngLat = positionLngLat;
        }

        public int getVisitCount() {
            return visitCount;
        }

        public void setVisitCount(int visitCount) {
            this.visitCount = visitCount;
        }

        public String getWebsiteUrl() {
            return websiteUrl;
        }

        public void setWebsiteUrl(String websiteUrl) {
            this.websiteUrl = websiteUrl;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCharacteristicDesc() {
            return characteristicDesc;
        }

        public void setCharacteristicDesc(String characteristicDesc) {
            this.characteristicDesc = characteristicDesc;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public int getViewCount() {
            return viewCount;
        }

        public void setViewCount(int viewCount) {
            this.viewCount = viewCount;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTravelGuid() {
            return travelGuid;
        }

        public void setTravelGuid(String travelGuid) {
            this.travelGuid = travelGuid;
        }

        public int getPlayDays() {
            return playDays;
        }

        public void setPlayDays(int playDays) {
            this.playDays = playDays;
        }
    }
}
