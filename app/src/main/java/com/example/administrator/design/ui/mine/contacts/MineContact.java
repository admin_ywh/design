package com.example.administrator.design.ui.mine.contacts;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;

public interface MineContact {
    interface view extends BaseView{

        void Successs(int data);

        void againLogin();
    }
    interface presenter extends BasePresenter{

        void updateUser(int position);
    }
}
