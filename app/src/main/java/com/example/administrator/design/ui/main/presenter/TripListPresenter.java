package com.example.administrator.design.ui.main.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.base.mvpbase.baseImpl.BasePresenterImpl;
import com.example.administrator.design.bean.TestBean;
import com.example.administrator.design.retroft.Api;
import com.example.administrator.design.retroft.ExceptionHelper;
import com.example.administrator.design.ui.main.contacts.TripListContacts;
import com.orhanobut.hawk.Hawk;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TripListPresenter extends BasePresenterImpl<TripListContacts.view>
        implements TripListContacts.presenter {
  private TestBean bean;
    public TripListPresenter(TripListContacts.view view) {
        super(view);
        bean= Hawk.get("testBean");
    }

    @SuppressLint("CheckResult")
    @Override
    public void tripList() {
        Api.getInstance().tripList(view.getToken(),view.getDataFlag())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tripListBeanBaseBean -> {
                    //处理服务器返回的数据
                    if (tripListBeanBaseBean.getCode().equals("0"))
                    {//处理服务器返回的错误信息
                       view.LoadData(tripListBeanBaseBean.getData().getTravelPlanList());
                    } else  if(tripListBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }{
                        Log.e("Throwable", tripListBeanBaseBean.getMsg());
                        ToastUtils.showShort(tripListBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, ExceptionHelper::handleException);
    }

    @SuppressLint("CheckResult")
    @Override
    public void joinTrip(int planId) {
        Api.getInstance().joinTrip(view.getToken(),planId+"",bean.getUserId()+"")
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(model -> {
                    //处理服务器返回的数据
                    if (model.getCode().equals("0"))
                    {//处理服务器返回的错误信息
                       view.Success();
                    } else if(model.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }{
                        Log.e("Throwable", model.getMsg());
                        ToastUtils.showShort(model.getMsg());
                    }
                    //处理网络异常
                }, ExceptionHelper::handleException);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onRefresh() {
        Api.getInstance().tripList(view.getToken(),view.getDataFlag())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tripListBeanBaseBean -> {
                    //处理服务器返回的数据
                    if (tripListBeanBaseBean.getCode().equals("0"))
                    {//处理服务器返回的错误信息
                        view.LoadData(tripListBeanBaseBean.getData().getTravelPlanList());
                        view.finishRefresh();
                    } else if(tripListBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }{
                        Log.e("Throwable", tripListBeanBaseBean.getMsg());
                        ToastUtils.showShort(tripListBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, ExceptionHelper::handleException);
    }

    @SuppressLint("CheckResult")
    @Override
    public void NojoinTrip(int planId) {
        Api.getInstance().cancelJoinTravelPlan(view.getToken(),planId+"",bean.getUserId()+"")
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(model -> {
                    //处理服务器返回的数据
                    if (model.getCode().equals("0"))
                    {//处理服务器返回的错误信息
                        view.CancelSuccess();
                    } else if(model.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }else {
                        Log.e("Throwable", model.getMsg());
                        ToastUtils.showShort(model.getMsg());
                    }
                    //处理网络异常
                }, ExceptionHelper::handleException);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onRefreshMy() {
        Api.getInstance().tripList(view.getToken(),view.getDataFlag())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tripListBeanBaseBean -> {
                    //处理服务器返回的数据
                    if (tripListBeanBaseBean.getCode().equals("0"))
                    {//处理服务器返回的错误信息
                        view.LoadData(tripListBeanBaseBean.getData().getTravelPlanList());
                        view.finishRefresh();
                    } else if(tripListBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }{
                        Log.e("Throwable", tripListBeanBaseBean.getMsg());
                        ToastUtils.showShort(tripListBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, ExceptionHelper::handleException);
    }
}
