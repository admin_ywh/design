package com.example.administrator.design.ui.main.adapter;

import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.administrator.design.R;
import com.example.administrator.design.bean.ScenicSpotBean;

import java.util.List;

import butterknife.BindView;

public class AddAdapter extends BaseQuickAdapter<ScenicSpotBean.ScenicspotBean, BaseViewHolder> {
    @BindView(R.id.iv_pic)
    ImageView ivPic;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_day)
    TextView tvDay;
    @BindView(R.id.btn_want)
    Button btnWant;

    public AddAdapter(@Nullable List<ScenicSpotBean.ScenicspotBean> data) {
        super(R.layout.layout_add_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ScenicSpotBean.ScenicspotBean item) {
        ImageView ivPic = helper.getView(R.id.iv_pic);
        helper.addOnClickListener(R.id.iv_pic);
        helper.addOnClickListener(R.id.btn_want);
        Glide.with(mContext).load(item.getImgUrls()).into(ivPic);
        helper.setText(R.id.tv_title, item.getName());
        helper.setText(R.id.tv_day, "people：" + item.getViewCount() + "       " + "days:" + item.getPlayDays());

    }
}
