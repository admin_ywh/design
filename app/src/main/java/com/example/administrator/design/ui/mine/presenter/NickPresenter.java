package com.example.administrator.design.ui.mine.presenter;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.base.mvpbase.baseImpl.BasePresenterImpl;
import com.example.administrator.design.retroft.Api;
import com.example.administrator.design.retroft.ExceptionHelper;
import com.example.administrator.design.ui.mine.contacts.Nickcontact;
import com.orhanobut.hawk.Hawk;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NickPresenter extends BasePresenterImpl<Nickcontact.view> implements Nickcontact.presenter {
    public NickPresenter(Nickcontact.view view) {
        super(view);
    }


    @SuppressLint("CheckResult")
    @Override
    public void setUserName() {
        String name=view.getNickName();
        if(TextUtils.isEmpty(name)){
            ToastUtils.showShort("Modified nickname can not be empty");
        }else{
            Api.getInstance().updateUser(Hawk.get("token"),name,"","")
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe(disposable -> {
                        addDisposable(disposable);
                        view.showLoadingDialog();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(userBeanBaseBean -> {
                        //处理服务器返回的数据
                        view.dismissLoadingDialog();
                        if (userBeanBaseBean.getCode().equals("0"))
                        {//处理服务器返回的错误信息
                            view.Successs(userBeanBaseBean.getData());
                        } else {
                            Log.e("Throwable", userBeanBaseBean.getMsg());
                            ToastUtils.showShort(userBeanBaseBean.getMsg());
                        }
                        //处理网络异常
                    }, throwable -> {
                        view.dismissLoadingDialog();
                        ExceptionHelper.handleException(throwable);
                    });
        }

    }
}
