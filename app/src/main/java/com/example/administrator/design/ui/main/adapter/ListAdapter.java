package com.example.administrator.design.ui.main.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.administrator.design.R;
import com.example.administrator.design.bean.RecommendScenicSpotBean;
import com.example.administrator.design.bean.ScenicSpotBean;

import java.util.List;

public class ListAdapter extends BaseQuickAdapter<RecommendScenicSpotBean.ScenicspotBean, BaseViewHolder> {
    public ListAdapter(@Nullable List<RecommendScenicSpotBean.ScenicspotBean> data) {
        super(R.layout.layout_list_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, RecommendScenicSpotBean.ScenicspotBean item) {
        ImageView ivPic = helper.getView(R.id.iv_pic);
        helper.addOnClickListener(R.id.iv_pic);
        helper.setText(R.id.tv_title, item.getName());
        Glide.with(mContext).load(item.getImgUrls()).into(ivPic);
        helper.setText(R.id.tvFee, "Number:" + item.getCost());
        helper.setText(R.id.tv_day, "Days:" + item.getPlayDays());
    }

}
