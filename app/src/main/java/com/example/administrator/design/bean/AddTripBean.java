package com.example.administrator.design.bean;

public class AddTripBean {
    private String planId;

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }
}
