package com.example.administrator.design.ui.main.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.administrator.design.R;
import com.example.administrator.design.bean.CityBean;

import java.util.List;

public class MoreAdapter extends BaseQuickAdapter<CityBean.CitysBean,BaseViewHolder> {
    public MoreAdapter(@Nullable List<CityBean.CitysBean> data) {
        super(R.layout.layout_more_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CityBean.CitysBean item) {
       helper.setText(R.id.tv_city,item.getCityName());
    }


}
