package com.example.administrator.design.ui.main.listener;

import android.view.View;

public interface OnItemClickLitener {
	
	void onItemClick(View view, int position, String name, String cityName);
	void onItemLongClick(View view, int position);
}