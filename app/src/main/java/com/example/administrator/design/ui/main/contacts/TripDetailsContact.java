package com.example.administrator.design.ui.main.contacts;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;
import com.example.administrator.design.bean.TripDetailsBean;

public interface TripDetailsContact {
    interface view extends BaseView{

        String getPlanId();

        void LoadData(TripDetailsBean data);

        void againLogin();

        void pushSuccess();
    }
    interface presenter extends BasePresenter{

        void getTravelPlanDetail();

        void pushTrip();

        void RevertpushTrip();
    }
}
