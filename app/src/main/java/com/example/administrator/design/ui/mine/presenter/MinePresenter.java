package com.example.administrator.design.ui.mine.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.example.administrator.design.base.mvpbase.baseImpl.BasePresenterImpl;
import com.example.administrator.design.retroft.Api;
import com.example.administrator.design.retroft.ExceptionHelper;
import com.example.administrator.design.ui.mine.contacts.MineContact;
import com.orhanobut.hawk.Hawk;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MinePresenter extends BasePresenterImpl<MineContact.view> implements MineContact.presenter {

    public MinePresenter(MineContact.view view) {
        super(view);
    }


    @SuppressLint("CheckResult")
    @Override
    public void updateUser(int position) {

        Api.getInstance().updateUser(Hawk.get("token"),"","",position+"")
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userBeanBaseBean -> {
                    //处理服务器返回的数据
                    if (userBeanBaseBean.getCode().equals("0"))
                    {//处理服务器返回的错误信息
                         view.Successs(position);
                    } else  if(userBeanBaseBean.getMsg().trim().equals(" Login Token is invalid, please check whether Token is correct!")){
                        view.againLogin();
                    }else {
                        Log.e("Throwable", userBeanBaseBean.getMsg());
                        ToastUtils.showShort(userBeanBaseBean.getMsg());
                    }
                    //处理网络异常
                }, ExceptionHelper::handleException);
    }
    }
