package com.example.administrator.design.ui.main.contacts;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;
import com.example.administrator.design.bean.CityBean;
import com.example.administrator.design.bean.RecommendScenicSpotBean;
import com.example.administrator.design.bean.ScenicSpotBean;

import java.util.List;

public interface HomeContact {
    interface homeView extends BaseView{

        String getToken();

        void setLoadData(List<CityBean.CitysBean> data);


        void setDataSpot(List<RecommendScenicSpotBean.ScenicspotBean> scenicspot);

        void againLogin();
    }
    interface presenter extends BasePresenter{

        void getAppRecommendScenicSpot();

        void getCityList();
    }
}
