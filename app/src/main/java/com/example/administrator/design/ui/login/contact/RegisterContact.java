package com.example.administrator.design.ui.login.contact;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.BaseView;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseBean;
import com.example.administrator.design.bean.TestBean;

public interface RegisterContact {
    interface RegisterView extends BaseView{

        String getPhone();

        String getPsd();

        String getSubmitPsd();

        String getUserName();

        void success(BaseBean<TestBean> storiesBeen);
    }
    interface RegisterPresenter extends BasePresenter{

        void register();
    }
}
