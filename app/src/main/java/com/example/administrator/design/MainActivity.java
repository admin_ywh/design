package com.example.administrator.design;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.bean.CityBean;
import com.example.administrator.design.ui.main.fragment.HomeFragment;
import com.example.administrator.design.ui.main.fragment.MineFragment;
import com.example.administrator.design.ui.main.fragment.TripFragment;
import com.example.administrator.design.widget.NoScrollViewPager;

import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    @BindView(R.id.home_view_pager)
    NoScrollViewPager homeViewPager;
    @BindView(R.id.home_tab_layout)
    TabLayout homeTabLayout;
    private String[] mTitles = new String[]{"main", "trip", "mine"};
    private long mExitTime;
    private int[] imgs = {R.mipmap.onenormal, R.mipmap.twonormal, R.mipmap.threenormal};

    private List<CityBean.CitysBean> list;
    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initViews() {
        //Fragment页面加载
        homeViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                if (position == 1) {
                    return new TripFragment();
                } else if (position == 2) {
                    return new MineFragment();
                }
                return new HomeFragment();
            }

            @Override
            public int getCount() {
                return mTitles.length;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mTitles[position];
            }

        });
        //TabLayout绑定ViewPager
        homeTabLayout.setupWithViewPager(homeViewPager);
        //预加载全部
        homeViewPager.setOffscreenPageLimit(3);
        homeTabLayout.getTabAt(0).setCustomView(getTabView(0));
        homeTabLayout.getTabAt(1).setCustomView(getTabView(1));
        homeTabLayout.getTabAt(2).setCustomView(getTabView(2));

        homeTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab == homeTabLayout.getTabAt(0)) {
                    ((ImageView) tab.getCustomView().findViewById(R.id.iv_icon)).setImageResource(R.mipmap.onepress);
                    ((TextView) tab.getCustomView().findViewById(R.id.tv_title)).setTextColor(getResources().getColor(R.color.colorAccent));
                    homeViewPager.setCurrentItem(0);
                } else if (tab == homeTabLayout.getTabAt(1)) {
                    ((ImageView) tab.getCustomView().findViewById(R.id.iv_icon)).setImageResource(R.mipmap.twopress);
                    ((TextView) tab.getCustomView().findViewById(R.id.tv_title)).setTextColor(getResources().getColor(R.color.colorAccent));
                    homeViewPager.setCurrentItem(1);
                } else if (tab == homeTabLayout.getTabAt(2)) {
                    ((ImageView) tab.getCustomView().findViewById(R.id.iv_icon)).setImageResource(R.mipmap.threepress);
                    ((TextView) tab.getCustomView().findViewById(R.id.tv_title)).setTextColor(getResources().getColor(R.color.colorAccent));
                    homeViewPager.setCurrentItem(2);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab == homeTabLayout.getTabAt(0)) {
                    ((ImageView) tab.getCustomView().findViewById(R.id.iv_icon)).setImageResource(R.mipmap.onenormal);
                    ((TextView) tab.getCustomView().findViewById(R.id.tv_title)).setTextColor(getResources().getColor(R.color.colorGray));
                } else if (tab == homeTabLayout.getTabAt(1)) {
                    ((ImageView) tab.getCustomView().findViewById(R.id.iv_icon)).setImageResource(R.mipmap.twonormal);
                    ((TextView) tab.getCustomView().findViewById(R.id.tv_title)).setTextColor(getResources().getColor(R.color.colorGray));
                } else if (tab == homeTabLayout.getTabAt(2)) {
                    ((ImageView) tab.getCustomView().findViewById(R.id.iv_icon)).setImageResource(R.mipmap.threenormal);
                    ((TextView) tab.getCustomView().findViewById(R.id.tv_title)).setTextColor(getResources().getColor(R.color.colorGray));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private View getTabView(int position) {

        View view = View.inflate(this, R.layout.tab_item, null);
        TextView tvTitle = view.findViewById(R.id.tv_title);
        ImageView ivIcon = view.findViewById(R.id.iv_icon);

        tvTitle.setText(mTitles[position]);
        ivIcon.setImageResource(imgs[position]);

        if (position == 0) {
            tvTitle.setTextColor(getResources().getColor(R.color.colorAccent));
            ivIcon.setImageResource(R.mipmap.onepress);
        }
        return view;
    }
    //对返回键进行监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    //退出方法
    private void exit() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            Toast.makeText(MainActivity.this, "再按一次退出应用", Toast.LENGTH_SHORT).show();
            mExitTime = System.currentTimeMillis();
        } else {
            //用户退出处理
            finish();
            System.exit(0);
        }
    }
}
