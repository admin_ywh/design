package com.example.administrator.design.ui.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.example.administrator.design.MainActivity;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.BasePresenter;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.ui.login.activity.LoginActivity;
import com.orhanobut.hawk.Hawk;

import java.util.Timer;
import java.util.TimerTask;

public class SplanActivity extends Activity {

    Timer timer=new Timer();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splan);
        initViews();
    }

    private void initViews() {
        timer.schedule(new TimerTask() {
           @Override
           public void run() {
             if(Hawk.get("token")!=null){
                 startActivity(new Intent(SplanActivity.this, MainActivity.class));
             }else {
                 startActivity(new Intent(SplanActivity.this, LoginActivity.class));
             }
             finish();
           }
       },2000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }
}
