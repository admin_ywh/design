package com.example.administrator.design.ui.login.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.administrator.design.MainActivity;
import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.bean.TestBean;
import com.example.administrator.design.ui.login.RegisterActivity;
import com.example.administrator.design.ui.login.contact.LoginContact;
import com.example.administrator.design.ui.login.presenter.LoginPresenter;
import com.example.administrator.design.ui.main.activity.SelectCityActivity;
import com.example.administrator.design.ui.main.adapter.SelectCityAdapter;
import com.example.administrator.design.utils.SharePreUtils;
import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity<LoginContact.presenter> implements LoginContact.view {

    @BindView(R.id.login)
    Button login;
    @BindView(R.id.rl_top)
    RelativeLayout rlTop;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.tv_register)
    TextView tvRegister;

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    /**
     * init presenter
     *
     * @return presenter
     */
    @Override
    public LoginContact.presenter initPresenter() {
        return new LoginPresenter(this);
    }


    @Override
    protected void initViews() {

    }


    @Override
    public void getLoadData(TestBean testBean) {
        Hawk.put("testBean", testBean);
        startActivity(new Intent(this, SelectCityActivity.class));
        finish();
    }

    @Override
    public String getUserName() {
        return username.getText().toString().trim();
    }

    @Override
    public String getPsd() {
        return password.getText().toString().trim();
    }


    @OnClick({R.id.login, R.id.tv_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login:
                presenter.login();
                break;
            case R.id.tv_register:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }
}
