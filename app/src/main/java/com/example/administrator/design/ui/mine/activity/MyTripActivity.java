package com.example.administrator.design.ui.mine.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.example.administrator.design.R;
import com.example.administrator.design.base.mvpbase.baseImpl.BaseActivity;
import com.example.administrator.design.bean.TripListBean;
import com.example.administrator.design.ui.login.activity.LoginActivity;
import com.example.administrator.design.ui.main.TripDetailsActivity;
import com.example.administrator.design.ui.main.adapter.MyTripAdapter;
import com.example.administrator.design.ui.main.contacts.TripListContacts;
import com.example.administrator.design.ui.main.presenter.TripListPresenter;
import com.example.administrator.design.utils.ActivityManager;
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;
import com.lcodecore.tkrefreshlayout.header.progresslayout.ProgressLayout;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MyTripActivity extends BaseActivity<TripListContacts.presenter> implements
        TripListContacts.view {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tripList)
    RecyclerView tripList;
    @BindView(R.id.refreshLayout)
    TwinklingRefreshLayout refreshLayout;
    private MyTripAdapter tripAdapter;
    private List<TripListBean.TravelPlanListBean> data = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_my_trip;
    }

    @Override
    public TripListContacts.presenter initPresenter() {
        return new TripListPresenter(this);
    }

    @Override
    protected void initViews() {

        ProgressLayout headView = new ProgressLayout(this);
        headView.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent), ContextCompat.getColor(this, R.color.yellow),
                ContextCompat.getColor(this, R.color.Green));
        refreshLayout.setHeaderView(headView);
        refreshLayout.setOnRefreshListener(new RefreshListenerAdapter() {
            @Override
            public void onRefresh(TwinklingRefreshLayout refreshLayout) {
                super.onRefresh(refreshLayout);
                presenter.onRefreshMy();
            }

            @Override
            public void onLoadMore(TwinklingRefreshLayout refreshLayout) {
                super.onLoadMore(refreshLayout);
                new Handler().postDelayed(() -> refreshLayout.finishLoadmore(), 1500);
            }
        });
        tripList.setLayoutManager(new LinearLayoutManager(this));
        tripAdapter = new MyTripAdapter(data);
        tripList.setAdapter(tripAdapter);

        tripAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            if(view.getId()==R.id.iv_pic){
                startActivity(new Intent(MyTripActivity.this,TripDetailsActivity.class)
                        .putExtra("planId",tripAdapter.getItem(position).getPlanId())
                        .putExtra("title",tripAdapter.getItem(position).getPlanName())
                        .putExtra("time",tripAdapter.getItem(position).getTime()));
            }
            });
    }


    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        this.finish();
    }

    @Override
    public String getToken() {
        return Hawk.get("token").toString();
    }

    @Override
    public void LoadData(List<TripListBean.TravelPlanListBean> travelPlanList) {
        tripAdapter.setNewData(travelPlanList);
    }

    @Override
    public String getDataFlag() {
        return "3";
    }

    @Override
    public void Success() {

    }

    @Override
    public void finishRefresh() {
        new Handler().postDelayed(() ->  refreshLayout.finishRefreshing(), 1000);
    }

    @Override
    public void CancelSuccess() {

    }

    @Override
    public void againLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        ActivityManager.getAppInstance().finishAllActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.tripList();
    }
}
