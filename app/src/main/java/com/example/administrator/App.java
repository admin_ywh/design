package com.example.administrator;

import android.app.Application;
import android.content.Context;

import com.blankj.utilcode.util.Utils;
import com.orhanobut.hawk.Hawk;

public class App extends Application {
 public static Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();
        mContext=this;
        Hawk.init(mContext).build();
        Utils.init(mContext);
    }
}
